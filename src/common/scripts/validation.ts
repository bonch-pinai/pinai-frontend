interface Data {
    name?: string,
    surname?: string,
    student_card?: string | number,
    email?: string,
    password?: string,
    password_confirmation?: string
    new_password?: string;
    new_password_confirmation?: string;

    avatar?: string;
    contacts?: {
        inst?: string;
        telegram?: string;
        vk?: string;
    };
    settings?: {
        deadline?: boolean;
        notification?: boolean;
    }
}

export default function validation(
    formData: Data,
    formErrorData: Data,
    name: string,
    value: string,
    setFormErrorData: (props: any) => any
) : any {

    // eslint-disable-next-line
    const emailRegex = new RegExp("^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$");
    const passwordRegex = new RegExp("^(?=.*[0-9]+.*)(?=.*[a-zA-Z]+.*)[0-9a-zA-Z]{8,}$");
    const nameRegex = new RegExp("^[а-яА-ЯёЁa-zA-Z]{2,}$");
    const studentIDRegex = new RegExp("^[0-9]{7}");


    if (value) {

        switch (name) {
            case "email":

                !emailRegex.test(value) ?
                    setFormErrorData({...formErrorData, [name]: "Некорректный e-mail"}) :
                    setFormErrorData({...formErrorData, [name]: ""});

                break;

            case "new_password":
            case "password":

                !passwordRegex.test(value) ?
                    // setFormErrorData({...formErrorData, [name]: "Латинские символы, мин. 8 знаков, одна буква и цифра"}) :
                    setFormErrorData({...formErrorData, [name]: "Пароль не соответствует требованиям"}) :
                    setFormErrorData({...formErrorData, [name]: ""});

            break;

            case "password_confirmation":

                (value !== formData.password) ?
                    setFormErrorData({...formErrorData, [name]: "Не совпадает с паролем"}) :
                    setFormErrorData({...formErrorData, [name]: ""});

                break;

            case "new_password_confirmation":

                (value !== formData.new_password) ?
                    setFormErrorData({...formErrorData, [name]: "Не совпадает с новым паролем"}) :
                    setFormErrorData({...formErrorData, [name]: ""});

                break;

            case "name":

                !nameRegex.test(value) ?
                    setFormErrorData({...formErrorData, [name]: "Буквы, мин. 2 символа"}) :
                    setFormErrorData({...formErrorData, [name]: ""});

                break;

            case "surname":

                !nameRegex.test(value) ?
                    setFormErrorData({...formErrorData, [name]: "Буквы, мин. 2 символа"}) :
                    setFormErrorData({...formErrorData, [name]: ""});

                break;

            case "student_card":

                !studentIDRegex.test(value) ?
                    setFormErrorData({...formErrorData, [name]: "7 цифр"}) :
                    setFormErrorData({...formErrorData, [name]: ""});

                break;
        }
    } else {
        setFormErrorData({...formErrorData, [name]: ""});
    }
}