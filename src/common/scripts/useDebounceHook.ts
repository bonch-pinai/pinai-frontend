import { useEffect, useState } from "react";

export default function useDebounce(value: any, delay: any): any {

    const [debounceValue, setDebounceValue] = useState();

    useEffect(() => {

        const timeHandler = setTimeout(() => {
            setDebounceValue(value)
        }, delay);

        return () => {
            clearTimeout(timeHandler);
        }
    }, [value]); // eslint-disable-line react-hooks/exhaustive-deps

    return debounceValue
}