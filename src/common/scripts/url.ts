
export default class URLQuery {

    public static getParam(url: string, params: string[]) {

        const urlObject = new URL(url);
        const query = new URLSearchParams(urlObject.search);
        const paramsValues: {[key: string]: string} = {};

        // tslint:disable-next-line:prefer-const
        for (let key of params) {
            if (key) {
                const value = query.get(key);
                if (key && value) {
                    paramsValues[key] = value;
                    // Object.assign(paramReturn, {[key]: value});
                }
            }
        }

        console.log(paramsValues);

        return paramsValues;
    }
}