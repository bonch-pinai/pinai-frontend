import React from "react";
import { Link } from "react-router-dom";

interface IUserData {
    user_id: number;
    student_card: string;
    email: string;
    name: string;
    surname: string;
    avatar: string;
    contacts: {
        inst: string;
        telegram: string;
        vk: string;
    };
    settings: {
        deadline: boolean;
        notification: boolean;
    }
}

interface IProps {
    userData: IUserData;
    path: string;
    handleExit: (event: any) => any;
}

export default function SidebarComponent(props: Readonly<IProps>) {

    const currentLocation = window.location.href;

    return (

        <div
            className="custom-sidebar"
        >
            <div
                className="custom-sidebar-user"
            >
                <div
                    className="custom-sidebar-user-avatar"
                    style = {{ backgroundImage: `url(${props.userData.avatar})` }}
                />
                <div
                    className="custom-sidebar-user-info"
                >
                    {props.userData.name} <br />
                    {props.userData.surname}
                </div>
            </div>
            <Link
                to={`${props.path}/${props.userData.user_id}/dashboards`}
                className={
                    (currentLocation.includes("dashboard") && !currentLocation.includes("team")) ?
                    "custom-sidebar-link-active" :
                    "custom-sidebar-link"
                }
            >
                <svg viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg">
                    <path d="M0 0h24v24H0V0z" fill="none" />
                    <path d="M4 13h6c.55 0 1-.45 1-1V4c0-.55-.45-1-1-1H4c-.55 0-1 .45-1 1v8c0 .55.45 1 1 1zm0 8h6c.55 0 1-.45 1-1v-4c0-.55-.45-1-1-1H4c-.55 0-1 .45-1 1v4c0 .55.45 1 1 1zm10 0h6c.55 0 1-.45 1-1v-8c0-.55-.45-1-1-1h-6c-.55 0-1 .45-1 1v8c0 .55.45 1 1 1zM13 4v4c0 .55.45 1 1 1h6c.55 0 1-.45 1-1V4c0-.55-.45-1-1-1h-6c-.55 0-1 .45-1 1z" />
                </svg>
                Доски
            </Link>
            <Link
                to={`${props.path}/${props.userData.user_id}/tasks`}
                className={
                    (currentLocation.includes("tasks") && !currentLocation.includes("team")) ?
                    "custom-sidebar-link-active" :
                    "custom-sidebar-link"
                }
            >
                <svg viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg">
                    <path d="M0 0h24v24H0z" fill="none" />
                    <path d="M19 3h-4.18C14.4 1.84 13.3 1 12 1c-1.3 0-2.4.84-2.82 2H5c-1.1 0-2 .9-2 2v14c0 1.1.9 2 2 2h14c1.1 0 2-.9 2-2V5c0-1.1-.9-2-2-2zm-7 0c.55 0 1 .45 1 1s-.45 1-1 1-1-.45-1-1 .45-1 1-1zm2 14H7v-2h7v2zm3-4H7v-2h10v2zm0-4H7V7h10v2z" />
                </svg>
                Задачи
            </Link>
            <Link
                to={`${props.path}/${props.userData.user_id}/teams`}
                className={
                    currentLocation.includes("team") ? "custom-sidebar-link-active" :
                    "custom-sidebar-link"
                }
            >
                <svg fill="black" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg">
                    <rect width="24" height="24" fill="none" />
                    <path d="m12 12.75c1.63 0 3.07 0.39 4.24 0.9 1.08 0.48 1.76 1.56 1.76 2.73v1.62h-12v-1.61c0-1.18 0.68-2.26 1.76-2.73 1.17-0.52 2.61-0.91 4.24-0.91zm-8 0.25c1.1 0 2-0.9 2-2s-0.9-2-2-2-2 0.9-2 2 0.9 2 2 2zm1.13 1.1c-0.37-0.06-0.74-0.1-1.13-0.1-0.99 0-1.93 0.21-2.78 0.58-0.74 0.32-1.22 1.04-1.22 1.85v1.57h4.5v-1.61c0-0.83 0.23-1.61 0.63-2.29zm14.87-1.1c1.1 0 2-0.9 2-2s-0.9-2-2-2-2 0.9-2 2 0.9 2 2 2zm4 3.43c0-0.81-0.48-1.53-1.22-1.85-0.85-0.37-1.79-0.58-2.78-0.58-0.39 0-0.76 0.04-1.13 0.1 0.4 0.68 0.63 1.46 0.63 2.29v1.61h4.5v-1.57zm-12-10.43c1.66 0 3 1.34 3 3s-1.34 3-3 3-3-1.34-3-3 1.34-3 3-3z" />
                </svg>

                Команды
            </Link>
            <Link
                to={`${props.path}/${props.userData.user_id}/settings`}
                className={
                    (currentLocation.includes("settings") && !currentLocation.includes("team")) ?
                    "custom-sidebar-link-active" :
                    "custom-sidebar-link"
                }
            >
                <svg viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg">
                    <path d="M0 0h24v24H0V0z" fill="none" />
                    <path d="M19.43 12.98c.04-.32.07-.64.07-.98s-.03-.66-.07-.98l2.11-1.65c.19-.15.24-.42.12-.64l-2-3.46c-.12-.22-.39-.3-.61-.22l-2.49 1c-.52-.4-1.08-.73-1.69-.98l-.38-2.65C14.46 2.18 14.25 2 14 2h-4c-.25 0-.46.18-.49.42l-.38 2.65c-.61.25-1.17.59-1.69.98l-2.49-1c-.23-.09-.49 0-.61.22l-2 3.46c-.13.22-.07.49.12.64l2.11 1.65c-.04.32-.07.65-.07.98s.03.66.07.98l-2.11 1.65c-.19.15-.24.42-.12.64l2 3.46c.12.22.39.3.61.22l2.49-1c.52.4 1.08.73 1.69.98l.38 2.65c.03.24.24.42.49.42h4c.25 0 .46-.18.49-.42l.38-2.65c.61-.25 1.17-.59 1.69-.98l2.49 1c.23.09.49 0 .61-.22l2-3.46c.12-.22.07-.49-.12-.64l-2.11-1.65zM12 15.5c-1.93 0-3.5-1.57-3.5-3.5s1.57-3.5 3.5-3.5 3.5 1.57 3.5 3.5-1.57 3.5-3.5 3.5z" />
                </svg>

                Настройки
            </Link>
            <Link
                to="/"
                className="custom-sidebar-link"
                onClick={props.handleExit}
            >
                <svg viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg">
                    <path d="M0 0h24v24H0V0z" fill="none" />
                    <path d="M10.79 16.29c.39.39 1.02.39 1.41 0l3.59-3.59c.39-.39.39-1.02 0-1.41L12.2 7.7c-.39-.39-1.02-.39-1.41 0-.39.39-.39 1.02 0 1.41L12.67 11H4c-.55 0-1 .45-1 1s.45 1 1 1h8.67l-1.88 1.88c-.39.39-.38 1.03 0 1.41zM19 3H5c-1.11 0-2 .9-2 2v3c0 .55.45 1 1 1s1-.45 1-1V6c0-.55.45-1 1-1h12c.55 0 1 .45 1 1v12c0 .55-.45 1-1 1H6c-.55 0-1-.45-1-1v-2c0-.55-.45-1-1-1s-1 .45-1 1v3c0 1.1.9 2 2 2h14c1.1 0 2-.9 2-2V5c0-1.1-.9-2-2-2z" />
                </svg>
                Выход
            </Link>
        </div>
    )
}
