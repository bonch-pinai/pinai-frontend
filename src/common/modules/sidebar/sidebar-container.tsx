import React from "react";
import SidebarComponent from "./sidebar-component";
import API from "src/api/api";
import { useHistory } from "react-router-dom";

interface IUserData {
    user_id: number;
    student_card: string;
    email: string;
    name: string;
    surname: string;
    avatar: string;
    contacts: {
        inst: string;
        telegram: string;
        vk: string;
    };
    settings: {
        deadline: boolean;
        notification: boolean;
    }
}

interface IProps {
    userData: IUserData;
    path: string;
}

export default function Sidebar(props: Readonly<IProps>) {

    const history = useHistory();

    function handleExit(event: any): void {
        event.preventDefault();
        API.post("logout").then(() => {
            history.push("/");
        });
    }

    return (

        <SidebarComponent
            userData={props.userData}
            path={props.path}
            handleExit={handleExit}
        />
    )
}
