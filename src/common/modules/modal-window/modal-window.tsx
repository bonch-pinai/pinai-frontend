import React, { useEffect, useRef, useState } from "react";
// import API from "src/api/api";

interface IProps {
    title?: string;
    button?: string;
    label?: string;
    maxLength?: number;

    handleClose: () => any;
    handleSubmit?: (value?: any) => any;
}

export default function ModalWindowModule(props: React.PropsWithChildren<IProps>) {

    const [value, setValue] = useState("");
    const ref = useRef<any>(null);

    useEffect(() => {
        document.addEventListener("mousedown", handleClose);

        return () => {
            document.removeEventListener("mousedown", handleClose);
          };
    }, [ref]); // eslint-disable-line react-hooks/exhaustive-deps

    function handleClose(event: any) {
        if (ref.current && !ref.current.contains(event.target)) {
            props.handleClose();
        }
    }

    function handleChange(event: any) {
        setValue(event.target.value);
    }

    function handleSubmit(event: any) {
        event.preventDefault();

        if (props.handleSubmit) {
            props.handleSubmit(value);
        }
    }

    if (!props.children) {
        return (
            <div
                className="custom-modal-window"
            >
                <form
                    className="custom-form"
                    onSubmit={handleSubmit}
                    ref={ref}
                >
                    {
                        props.title &&
                        <span className="custom-form-title">
                            {props.title}
                        </span>
                    }
                    <div className="custom-form-input-wrapper">
                        <input
                            type="text"
                            id="name"
                            onChange={handleChange}
                            autoFocus
                            required
                            autoComplete={"off"}
                            maxLength={
                                props.maxLength ? props.maxLength : 25
                            }
                        />
                        <label
                            htmlFor="name"
                        >
                            {props.label}
                        </label>
                    </div>
                    <button
                        className="custom-button"
                        type="submit"
                    >
                        {props.button}
                    </button>
                </form>
            </div>
        )
    } else {
        return (
            <div
                className="custom-modal-window"
            >
                <div
                    className="custom-modal-window-body"
                    ref={ref}
                >
                    {
                        props.title &&
                        <span className="custom-form-title">
                            {props.title}
                        </span>
                    }
                    <div
                        className="custom-modal-window-content"
                    >
                        {props.children}
                    </div>
                </div>
            </div>
        )
    }
}