import React from "react";

interface IProps {
    title?: string;
    triangle: "top" | "right" | "bottom" | "left"
}

const Tooltip : React.FC<IProps> = props => {

    return (
        <div
            className="custom-tooltip"
        >
            {/* {
                (props.triangle === "top") &&
                <div className="custom-tooltip-triangle-top" />
            }
            {
                (props.triangle === "left") &&
                <div className="custom-tooltip-triangle-left" />
            } */}
            {
                props.title ? props.title :
                props.children
            }
        </div>
    )
}

export default Tooltip