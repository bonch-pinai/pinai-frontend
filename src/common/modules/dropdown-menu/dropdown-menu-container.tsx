import React from "react";
import DropdownMenuComponent from "./dropdown-menu-component";

interface IUserData {
    user_id: number;
    student_card: string;
    email: string;
    name: string;
    surname: string;
    avatar: string;
    contacts: {
        inst: string;
        telegram: string;
        vk: string;
    };
    settings: {
        deadline: boolean;
        notification: boolean;
    }
}

interface IProps {
    userData: IUserData;
}

export default function DropdownMenu(props: Readonly<IProps>) {

    return (

        <DropdownMenuComponent
            userData={props.userData}
        />
    )
}
