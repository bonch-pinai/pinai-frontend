import React, { useState, useEffect } from "react";
import EmailResendComponent from "./email-resend-component";
import API from "../../api/api";
import validation from "../../common/scripts/validation";
import { useHistory } from "react-router-dom";
import Loader from "src/common/modules/loader/loader";

// interface IResend {
//     state: {
//         email: string
//     }
// }

interface IFormData {
    email: string
}

export default function EmailResend(props: any) {

    const initData = {
        email: ""
    }

    const [formData, setFormData] = useState<IFormData>(initData);
    const [formErrorData, setFormErrorData] = useState<IFormData>(initData);
    const [isCurrentEmail, changeCurrentEmail] = useState(true);
    const [showError, setShowError] = useState(false);
    const [isMessageSent, setIsMessageSent] = useState(false);
    const [isLoaded, setIsLoaded] = useState(false);

    const history = useHistory();

    useEffect(() => {

        // API.get("check").then(
        //     (result) => {
        //         console.log(result)
        //     },
        //     error => {
        //         console.log(error);
        //         history.push("/");
        //     }
        // );

        if (props && props.location &&
            props.location.state && props.location.state.email) {
            setIsLoaded(true);
        } else {
            history.push("/");
        }

        if (!isCurrentEmail) {
            validation(formData, formErrorData, "email", formData.email, setFormErrorData);
            setShowError(false);
        }
    },
        // eslint-disable-next-line
        [formData]
    );

    function handleChange(event: React.ChangeEvent<HTMLInputElement>): void {
        const target = event.target;
        const value = target.value;
        setFormData({...formData, [target.id]: value});
    }

    function handleChangeEmail(event: any) {
        event.preventDefault();
        changeCurrentEmail(false);
    }

    function handleSubmit(event: React.FormEvent<HTMLFormElement>): void {
        event.preventDefault();
        console.log(formData);
        console.log(formErrorData);

        const checkForm = Object.values(formErrorData).every(value => value === "");
        if (checkForm) {
            console.log("Делаю вид, что выслал запрос");
            // if (isCurrentEmail) {
            //     setFormData({...formData, ["email"]: props.location.state.email});
            // }
            console.log(formData);
            setIsMessageSent(false);
            // setIsMessageSent(true);
            if (formData.email) {
                API.post(
                    "verification/resend",
                    formData
                ).then(
                    (response) => {
                        console.log(response);
                        setIsMessageSent(true);
                    }
                ).catch (
                    (error) => {
                        console.log(error);
                        setFormErrorData({"email": "Аккаунта с данной почтой не существует"});
                        setShowError(true);
                    }
                );
            } else {
                API.post(
                    "verification/resend"
                ).then(
                    (response) => {
                        console.log(response);
                        setIsMessageSent(true);
                    }
                ).catch(
                    (error) => {
                        console.log(error);
                        setFormErrorData({"email": "Аккаунта с данной почтой не существует"});
                        setShowError(true);
                    }
                );
            }
        } else {
            setShowError(true);
        }
    }

    if (isLoaded) {
        return (
            <EmailResendComponent
                isMessageSent={isMessageSent}
                email={props.location.state.email}
                formData={formData.email}
                formErrorData={formErrorData.email}
                showError={showError}
                isCurrentEmail={isCurrentEmail}
                handleSubmit={handleSubmit}
                handleChange={handleChange}
                handleChangeEmail={handleChangeEmail}
            />
        )
    } else {
        return (
            <Loader />
        );
    }
}