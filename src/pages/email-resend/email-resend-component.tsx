import React from "react";
import { Link } from "react-router-dom";

interface Props {
    email: string;
    isMessageSent: boolean;
    formErrorData: string;
    formData: string;
    showError: boolean;
    isCurrentEmail: boolean;
    handleSubmit: (event: any) => any;
    handleChange: (event: React.ChangeEvent<HTMLInputElement>) => any;
    handleChangeEmail: (event: any) => any;
}

export default function EmailResendComponent (props: Readonly<Props>) {

    const input =
        <div
            className="custom-form-input-wrapper"
        >
            <input
                type="text"
                id="email"
                onChange={props.handleChange}
                required
                autoFocus
            />
            <label
                htmlFor="email"
            >
                E-mail
            </label>
        </div>;

    if (!props.isMessageSent) {

        return (

            <div
                className="custom-recover"
            >
                <form
                    className="custom-form"
                    onSubmit={props.handleSubmit}
                >
                    <span className="custom-form-title">
                        Подтверждение почты
                    </span>
                    {
                        props.isCurrentEmail &&
                        <div className="custom-form-info">
                            На
                            <span> {props.email}</span> было выслано письмо для подтверждения почты.
                            Если письмо не пришло, нажмите кнопку ниже
                        </div>
                    }
                    {
                        !props.isCurrentEmail &&
                        <div className="custom-form-info">
                            Укажите адрес электронной почты, на который следует выслать новое письмо
                            для подтверждения
                        </div>
                    }
                    {
                        !props.isCurrentEmail && input
                    }
                    {
                        (props.formErrorData.length > 0 && props.showError && props.formData) &&
                        <span className="custom-form-error">
                            {props.formErrorData}
                        </span>
                    }
                    <button
                        className="custom-button"
                        type="submit"
                    >
                        {
                            props.isCurrentEmail ? "Выслать письмо повторно" :
                            "Выслать письмо на новую почту"
                        }
                    </button>
                    {props.isCurrentEmail && <hr />}
                    {
                        props.isCurrentEmail &&
                        <div
                            className="custom-link"
                            onClick={props.handleChangeEmail}
                        >
                            Хочу изменить почту
                        </div>
                    }
                </form>
            </div>
        )
    } else {

        return (
            <div
                className="custom-recover"
            >
                <form
                    className="custom-form"
                >
                    <span className="custom-form-title">
                        Подтверждение почты
                    </span>
                    <div className="custom-form-info">
                        На почту <span>{props.formData}</span> было отправлено письмо для подтверждения
                    </div>
                    <hr />
                    <Link
                        to=""
                        className="custom-link"
                    >
                        Вернуться ко входу
                    </Link>
                </form>
            </div>
        )
    }
}
