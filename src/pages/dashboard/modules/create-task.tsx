import React, { useState } from "react";

interface IProps {

    members: any;
    columnID: number;

    handleCreateTask: (value: any) => any;
}

export default function CreateTaskModule(
    props: Readonly<IProps>
) {

    const [task, setTask] = useState<any>(
        {
            name: "",
            description: "",
            date: "",
            executorID: 0,
        }
    );

    const currentDate = new Date();
    const newDate = new Date(currentDate);
    newDate.setHours(currentDate.getHours() + 1);
    const minDeadlineDate = newDate.toISOString().slice(0, 16);

    function handleChange(event: any) {
        const name = event.target.id;
        const value = event.target.value;
        setTask({...task, [name]: value});
    }

    function handleCreateTask(event: any) {
        event.preventDefault();

        const date = Date.parse(task.date);
        const payload = {
            name: task.name,
            description: task.description,
            deadline: date,
            column_id: props.columnID,
            executor_id: task.executorID
        }

        props.handleCreateTask(payload);
    }

    return (
        <div
            className="custom-task-create"
        >
            <form>
                <div className="custom-form-input-wrapper">
                    <input
                        type="text"
                        id="name"
                        value={task.name}
                        onChange={handleChange}
                        required
                        autoComplete="off"
                        autoFocus
                    />
                    <label
                        htmlFor="name"
                    >
                        Название
                    </label>
                </div>
                <div className="custom-form-input-wrapper">
                    <div className="custom-task-create-title">
                        Срок выполнения
                    </div>
                    <input
                        type="datetime-local"
                        id="date"
                        min={minDeadlineDate}
                        onChange={handleChange}
                        required
                    />
                </div>
                <div
                    className="custom-select-wrapper"
                >
                    <div className="custom-task-create-title">
                        Исполнитель
                    </div>
                    <select
                        id="executorID"
                        onChange={handleChange}
                        value={task.executionerID}
                        required
                    >
                        <option
                            value={0}
                        />
                        {
                            props.members.map((
                                member: {
                                    avatar: string;
                                    full_name: string;
                                    user_id: number
                                },
                                index: number) => {
                                    return (
                                        <option key={index} value={member.user_id}>
                                            {member.full_name}
                                        </option>
                                    )
                                }
                            )
                        }
                    </select>
                </div>
                <div className="custom-task-create-title">
                    Описание
                </div>
                <div
                    className="custom-textarea-wrapper"
                >
                    <textarea
                        id="description"
                        value={task.description}
                        onChange={handleChange}
                    />
                </div>
                <button
                    className="custom-button"
                    onClick={handleCreateTask}
                >
                    Добавить задачу
                </button>
            </form>
        </div>
    )
}