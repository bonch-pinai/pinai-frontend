import React from "react";

interface IProps {
    task_id: number;
    name: string;

    redirectToTask: (value: any) => any;
}

export default function ColumnTaskModule(props: Readonly<IProps>) {

    function redirectToTask() {
        props.redirectToTask(props.task_id);
    }

    return (
        <div
            className="custom-dashboard-column-task-wrapper"
            onClick={redirectToTask}
        >
            {/* <div
                className="custom-dashboard-column-top"
            > */}
                {/* <div
                    className="custom-dashboard-column-task"
                > */}
                    {props.name}
                {/* </div> */}
            {/* </div> */}
        </div>
    )
}