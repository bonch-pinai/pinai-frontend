import React, { useEffect, useState } from "react";
import API from "src/api/api";
import useDebounce from "src/common/scripts/useDebounceHook";
import ColumnTaskModule from "./column-task";

interface IProps {
    dashboard: any;

    columnID: number;
    name: string;
    index: number;

    getDashboardInfo: () => any;
    handleOpenModalCreateTask: (id: number) => any;
    redirectToTask: (value: any) => any;
}

export default function ColumnModule(props: Readonly<IProps>) {

    const [columnName, setColumnName] = useState(props.name);
    const [isLoaded, setIsLoaded] = useState(false);
    const debouncedColumnName = useDebounce(columnName, 2000);

    function setLoaded() {
        setIsLoaded(true);
    }

    useEffect(() => {

        const timeHandler = setTimeout(() => {
            setLoaded()
        }, 2500);

        return () => {
            clearTimeout(timeHandler);
        }
    }, []);

    useEffect(() => {
        if (isLoaded) {
            handleColumnNameSubmit();
        }
    }, [debouncedColumnName]); // eslint-disable-line react-hooks/exhaustive-deps

    function handleColumnNameChange(event: any) {
        const value = event.target.value;
        setColumnName(value);
    }

    function handleColumnNameSubmit() {

        const payload = {
            name: columnName
        }

        API.put(
            `column/${props.columnID}`,
            payload
        ).then(
            () => {
                props.getDashboardInfo();
            }
        ).catch(
            (error) => {
                console.log(error);
            }
        )
    }

    function handleDeleteColumn() {
        API.delete(
            `column/${props.columnID}`
        ).then(
            () => {
                props.getDashboardInfo();
            }
        ).catch(
            (error) => {
                console.log(error);
            }
        )
    }

    function handleOpenModalCreateTask() {
        props.handleOpenModalCreateTask(props.columnID);
    }

    // function handleOpenModalCreateTask(task: string) {
    //     props.handleOpenModalCreateTask(task, props.columnID)
    // }

    return (
        <div
            className="custom-dashboard-column-wrapper"
        >
            <div
                className="custom-dashboard-column-top"
            >
                <input
                    className="custom-input-invisible"
                    value={columnName}
                    maxLength={25}
                    onChange={handleColumnNameChange}
                    autoComplete={"off"}
                />
                <div
                    className="custom-icons-cross-simple"
                    onClick={handleDeleteColumn}
                />
            </div>
            {
                props.dashboard.columns[props.index].tasks.map((
                    tasks: {
                        task_id: number;
                        name: string;
                    },
                    index: number) => {

                    return (
                        <div
                            key={index}
                            className="custom-dashboard-column-task"
                        >
                            <ColumnTaskModule
                                task_id={tasks.task_id}
                                name={tasks.name}
                                redirectToTask={props.redirectToTask}
                            />
                        </div>
                    );
                })
            }
            <div
                className="custom-dashboard-column-task-create"
                onClick={handleOpenModalCreateTask}
            >
                <div className="custom-icons-plus-small" />
                Новая задача
            </div>
        </div>
    )
}