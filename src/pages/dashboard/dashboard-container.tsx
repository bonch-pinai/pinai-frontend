import React, { useEffect, useState } from "react";
import { useHistory, useParams } from "react-router-dom";
import API from "src/api/api";
import useDebounce from "src/common/scripts/useDebounceHook";
import DashboardComponent from "./dashboard-component";

interface IProps {
    userID: number;
}

export default function Dashboard(props: Readonly<IProps>) {

    const [dashboard, setDashboard] = useState<any>({name: ""});
    const [prevName, setPrevName] = useState("");
    const [members, setMembers] = useState<any>();
    const [teamMembers, setTeamMembers] = useState<any>();
    const [membersID, setMembersID] = useState<any>();
    const [isLoaded, setIsLoaded] = useState(false);
    const [isTeamLoaded, setIsTeamLoaded] = useState(false);

    const history = useHistory();
    const { id } = useParams<{ id: string }>();
    const dashboardName = useDebounce(dashboard.name, 2000);

    async function getDashboardInfo(): Promise<void> {
        API.get(
            `dashboard/${id}`
        ).then(
            (response) => {
                const data = response.data.data;
                setDashboard(data);
                setPrevName(data.name);
                console.log(data);
                console.log(dashboard);
                setMembers(data.members);
                console.log(data.members)

                const membersArrayID = [];
                // tslint:disable-next-line:prefer-for-of
                for (let i = 0; i < data.members.length; i++) {
                    const memberID = data.members[i].user_id;
                    membersArrayID.push(memberID)
                }
                setMembersID(membersArrayID);
                console.log(membersID);

                setIsLoaded(true);

                getSubdivisionInfo(data.subdivision.subdivision_id);
            }
        ).catch(
            (error) => {
                console.log(error);
            }
        )

        return
    }

    async function getSubdivisionInfo(idTeam: number): Promise<void> {
        API.get(
            `subdivision/${idTeam}`
        ).then(
            (response) => {
                const data = response.data.data;
                setTeamMembers(data.members);
                setIsTeamLoaded(true);
            }
        ).catch(
            (error) => {
                console.log(error);
            }
        );

        return
    }

    useEffect(() => {
        getDashboardInfo();

    }, []); // eslint-disable-line react-hooks/exhaustive-deps

    useEffect(() => {

        if (isLoaded && dashboardName !== prevName && dashboardName !== "") {
            submitDashboardNameChange();
        }
        // } else {
        //     setDashboard({...dashboard, "name": prevName});
        // }

    }, [dashboardName]); // eslint-disable-line react-hooks/exhaustive-deps

    function handleDashboardNameChange(event: React.ChangeEvent<HTMLInputElement>) {

        const value = event.target.value;
        setDashboard({...dashboard, "name": value});
    }

    function submitDashboardNameChange() {

        let payloadName = "";
        if (!dashboardName) {
            payloadName = prevName;
        } else {
            payloadName = dashboardName;
        }

        const payload = {
            name: payloadName
        }

        API.put(
            `dashboard/${id}`,
            payload
        ).then(
            (response) => {
                const data = response.data.data;
                setDashboard(data);
                setPrevName(data.name);
                const membersArrayID = [];
                // tslint:disable-next-line:prefer-for-of
                for (let i = 0; i < data.members.length; i++) {
                    const memberID = data.members[i].user_id;
                    membersArrayID.push(memberID)
                }
                setMembersID(membersArrayID);
            }
        )
    }

    function handleDeleteDashboard () {
        API.delete(
            `dashboard/${id}`
        ).then(
            () => {
                history.push(`/user/team/${dashboard.subdivision.subdivision_id}`)
            }
        ).catch(
            (error) => {
                console.log(error)
            })
    }

    function handleDeleteMember (deleteID: number) {
        const data = membersID;
        const index = data.indexOf(deleteID);

        console.log(deleteID);
        console.log(index);

        if (index !== -1) {
            data.splice(index, 1);
            setMembersID(data);
        }

        const payload = {
            member_id: data
        }

        API.put(
            `dashboard/${id}`,
            payload
        ).then(
            (response) => {
                const membersData = response.data.data.members;
                console.log(response);
                setMembers(membersData);
            }
        ).catch(
            (error) => {
                console.log(error);
            }
        );
    }

    function handleAddMember (value: number) {
        const data = membersID;
        const addID = Number(value);
        data.push(addID);

        const payload = {
            member_id: data
        };

        API.put(
            `dashboard/${id}`,
            payload
        ).then(
            (response) => {
                const membersData = response.data.data.members;
                console.log(response);
                setMembers(membersData);
            }
        ).catch(
            (error) => {
                console.log(error);
            }
        )
    }

    function handleCreateNewColumn (value: any) {

        const payload = {
            name: value
        }

        console.log(payload);

        API.post(
            `column/${id}`,
            payload
        ).then(
            () => {
                getDashboardInfo();
            }
        ).catch(
            (error) => {
                console.log(error);
            }
        )
    }

    function handleCreateTask (payload: any) {

        API.post(
            `dashboard/${id}/task`,
            payload
        ).then(
            () => {
                getDashboardInfo();
            }
        ).catch(
            (error) => {
                console.log(error);
            }
        )
    }

    function redirectToTask(taskID: number) {
        history.push({
            pathname: `/user/dashboard/${dashboard.dashboard_id}/task/${taskID}`,
            state: {
                taskID
                // taskID: taskID,
                // creatorID: dashboard.creator.user_id,
                // leaderID: dashboard.leader_id
            }
        })
    }

    if (isLoaded) {
        return (
            <>
                <DashboardComponent
                    dashboard={dashboard}
                    isCreator={
                        dashboard.creator.user_id === props.userID
                    }
                    isLeader={
                        dashboard.subdivision.leader_id === props.userID
                    }
                    members={members}
                    membersID={membersID}
                    userID={props.userID}
                    teamMembers={teamMembers}
                    isTeamLoaded={isTeamLoaded}
                    getDashboardInfo={getDashboardInfo}
                    handleDashboardNameChange={handleDashboardNameChange}
                    handleDeleteDashboard={handleDeleteDashboard}
                    handleDeleteMember={handleDeleteMember}
                    handleAddMember={handleAddMember}
                    handleCreateNewColumn={handleCreateNewColumn}
                    handleCreateTask={handleCreateTask}
                    redirectToTask={redirectToTask}
                    // handleCreateNewTask={handleCreateNewTask}
                />
            </>
        )
    } else {
        return (
            <></>
        )
    }
}
