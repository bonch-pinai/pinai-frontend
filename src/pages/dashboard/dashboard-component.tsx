import React, { useEffect, useRef, useState } from "react";
import { Link } from "react-router-dom";
import ModalWindowModule from "src/common/modules/modal-window/modal-window";
import MemberModule from "../team/modules/member";
import ColumnModule from "./modules/column";
import CreateTaskModule from "./modules/create-task";

interface IProps {
    dashboard: any;
    teamMembers: any;
    isCreator: boolean;
    isLeader: boolean;
    members: any;
    membersID: any;
    userID: number;

    isTeamLoaded: boolean;

    getDashboardInfo: () => any;
    handleDashboardNameChange: (event: any) => any;
    handleDeleteDashboard: (event: any) => any;
    handleDeleteMember: (event: any) => any;
    handleAddMember: (event: any) => any;

    handleCreateNewColumn: (value: any) => any;
    handleCreateTask: (value: any) => any;

    redirectToTask: (value: number) => any;
}

export default function DashboardComponent(props: Readonly<IProps>) {

    const widthRef = useRef<any>(null);
    const [width, setWidth] = useState(0);
    const [isOpenModalMembers, setIsOpenModalMembers] = useState(false);
    const [isOpenModalAddMembers, setIsOpenModalAddMembers] = useState(false);
    const [isOpenModalCreateColumn, setIsOpenModalCreateColumn] = useState(false);
    const [isOpenModalCreateTask, setIsOpenModalCreateTask] = useState(false);

    const [currentColumn, setCurrentColumn] = useState<any>();

    function filterMembers(member: any) {
        if (props.membersID.includes(member.user_id)) {
            return false
        } else {
            return true;
        }
    }

    function openModalMembers() {
        setIsOpenModalMembers(true);
    }

    function closeModalMembers() {
        setIsOpenModalMembers(false);
    }

    function openModalAddMembers() {
        setIsOpenModalAddMembers(true);
    }

    function closeModalAddMembers() {
        setIsOpenModalAddMembers(false);
    }

    function openModalCreateColumn() {
        setIsOpenModalCreateColumn(true);
    }

    function closeModalCreateColumn() {
        setIsOpenModalCreateColumn(false);
    }

    function openModalCreateTask(columnID: number) {
        setCurrentColumn(columnID);
        setIsOpenModalCreateTask(true);
    }

    function closeModalCreateTask() {
        setIsOpenModalCreateTask(false);
    }

    function handleCreateNewColumn(value: string) {
        closeModalCreateColumn();
        props.handleCreateNewColumn(value);
    }

    function handleCreateTask(value: any) {
        closeModalCreateTask();
        props.handleCreateTask(value);
    }

    useEffect(() => {

        if (widthRef && widthRef.current) {
            setWidth(widthRef.current.offsetWidth)
        }
    }, [props.dashboard.name]);

    const style = {
        width: `${width}px`
    }

    return (
        <>
            <div
                className="custom-dashboard"
            >
                <div
                    className="custom-dashboard-wrapper"
                >
                    <div
                        className="custom-dashboard-topbar"
                    >
                        {
                            (props.isCreator || props.isLeader) &&
                            <input
                                className="custom-input-invisible"
                                value={props.dashboard.name}
                                style={style}
                                onChange={props.handleDashboardNameChange}
                            />
                        }
                        {
                            (props.isCreator || props.isLeader) &&
                            <div
                                className="custom-input-invisible-ctrl"
                                ref={widthRef}
                            >
                                {props.dashboard.name}
                            </div>
                        }
                        {
                            !(props.isCreator || props.isLeader) &&
                            <div
                                className="custom-dashboard-topbar-title-name"
                            >
                                {props.dashboard.name}
                            </div>
                        }
                        <div
                            className="custom-dashboard-topbar-vertical-ruler"
                        />
                        <Link
                            className="custom-dashboard-topbar-title-active"
                            to={`/user/team/${props.dashboard.subdivision.subdivision_id}`}
                        >
                            <svg fill="black" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg">
                                <rect width="24" height="24" fill="none" />
                                <path d="m12 12.75c1.63 0 3.07 0.39 4.24 0.9 1.08 0.48 1.76 1.56 1.76 2.73v1.62h-12v-1.61c0-1.18 0.68-2.26 1.76-2.73 1.17-0.52 2.61-0.91 4.24-0.91zm-8 0.25c1.1 0 2-0.9 2-2s-0.9-2-2-2-2 0.9-2 2 0.9 2 2 2zm1.13 1.1c-0.37-0.06-0.74-0.1-1.13-0.1-0.99 0-1.93 0.21-2.78 0.58-0.74 0.32-1.22 1.04-1.22 1.85v1.57h4.5v-1.61c0-0.83 0.23-1.61 0.63-2.29zm14.87-1.1c1.1 0 2-0.9 2-2s-0.9-2-2-2-2 0.9-2 2 0.9 2 2 2zm4 3.43c0-0.81-0.48-1.53-1.22-1.85-0.85-0.37-1.79-0.58-2.78-0.58-0.39 0-0.76 0.04-1.13 0.1 0.4 0.68 0.63 1.46 0.63 2.29v1.61h4.5v-1.57zm-12-10.43c1.66 0 3 1.34 3 3s-1.34 3-3 3-3-1.34-3-3 1.34-3 3-3z" />
                            </svg>
                            {props.dashboard.subdivision.name}
                        </Link>
                        <div
                            className="custom-dashboard-topbar-vertical-ruler"
                        />
                        <div
                            className="custom-dashboard-topbar-title-active"
                            onClick={openModalMembers}
                        >
                            Участники
                        </div>
                        {
                            (props.isCreator || props.isLeader) &&
                            <>
                                <div
                                    className="custom-dashboard-topbar-title-add"
                                    onClick={openModalAddMembers}
                                >
                                    <div className="custom-icons-plus-small" />
                                    Добавить
                                </div>
                                <div
                                    className="custom-dashboard-topbar-vertical-ruler"
                                />
                                <div
                                    className="custom-dashboard-topbar-title-delete"
                                    onClick={props.handleDeleteDashboard}
                                >
                                    Удалить доску
                                </div>
                            </>
                        }
                    </div>
                    <div
                        className="custom-dashboard-body"
                    >
                        {
                            props.dashboard.columns
                                .sort((a: any, b: any) => a.id - b.id)
                                .map((
                                column: {
                                    id: number;
                                    name: string;
                                },
                                index: number) => {

                                return (
                                    <div
                                        key={index}
                                        className="custom-dashboard-column"
                                    >
                                        <ColumnModule
                                            dashboard={props.dashboard}
                                            columnID={column.id}
                                            name={column.name}
                                            index={index}
                                            getDashboardInfo={props.getDashboardInfo}
                                            handleOpenModalCreateTask={openModalCreateTask}
                                            redirectToTask={props.redirectToTask}
                                        />
                                    </div>
                                );
                            })
                        }
                        <div
                            className="custom-dashboard-column-create"
                            onClick={openModalCreateColumn}
                        >
                            <div className="custom-icons-plus-small" />
                            Новая колонка
                        </div>
                    </div>

                </div>
            </div>
            {
                isOpenModalMembers &&
                <ModalWindowModule
                    title="Участники доски"
                    handleClose={closeModalMembers}
                >
                    <div className="custom-team-member-body">
                        {
                            props.members.map((
                                member: {
                                    avatar: string;
                                    full_name: string;
                                    user_id: number
                                },
                                index: number) => {

                                return (
                                    <div
                                        key={index}
                                        className="custom-team-member"
                                    >
                                        <MemberModule
                                            name={member.full_name}
                                            avatar={member.avatar}
                                            memberID={member.user_id}
                                            userID={props.userID}
                                            isLeader={props.isLeader}
                                            isCreator={props.isCreator}
                                            delete={true}
                                            additionalClass={"value-2"}
                                            handleMemberStatusChange={props.handleDeleteMember}
                                        />
                                    </div>
                                );
                            })
                        }
                    </div>
                </ModalWindowModule>
            }
            {
                isOpenModalAddMembers &&
                <ModalWindowModule
                    title="Добавить участников"
                    handleClose={closeModalAddMembers}
                >
                    <div className="custom-team-member-body">
                        {
                            props.teamMembers
                                .filter(filterMembers)
                                .map((
                                    member: {
                                        avatar: string;
                                        full_name: string;
                                        user_id: number
                                    },
                                    index: number) => {

                                    return (
                                        <div
                                            key={index}
                                            className="custom-team-member"
                                        >
                                            <MemberModule
                                                name={member.full_name}
                                                avatar={member.avatar}
                                                memberID={member.user_id}
                                                userID={props.userID}
                                                isLeader={props.isLeader}
                                                isCreator={props.isCreator}
                                                delete={false}
                                                additionalClass={"value-2"}
                                                handleMemberStatusChange={props.handleAddMember}
                                            />
                                        </div>
                                    );
                                }
                            )
                        }
                    </div>
                </ModalWindowModule>
            }
            {
                isOpenModalCreateColumn &&
                <ModalWindowModule
                    title={"Новая колонка"}
                    label={"Название"}
                    button={"Создать"}
                    maxLength={15}
                    handleClose={closeModalCreateColumn}
                    handleSubmit={handleCreateNewColumn}
                />
            }
            {
                isOpenModalCreateTask &&
                <ModalWindowModule
                    title={"Новая задача"}
                    handleClose={closeModalCreateTask}
                    // handleSubmit={handleCreateNewColumn}
                >
                    <CreateTaskModule
                        members={props.members}
                        columnID={currentColumn}
                        handleCreateTask={handleCreateTask}
                    />
                </ModalWindowModule>
            }
        </>
    )
}
