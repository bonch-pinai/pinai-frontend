import React from "react";
import { Link } from "react-router-dom";

interface IRegistrationFormData {
    name: string;
    surname: string;
    student_card: string;
    email: string;
    password: string;
    password_confirmation: string;
}

interface Props {
    formData: IRegistrationFormData;
    errorData: IRegistrationFormData;

    handleSubmit: (event: any) => any;
    handleChange: (event: React.ChangeEvent<HTMLInputElement>) => any;
}
export default function RegistrationForm (props: Readonly<Props>) {

    return (

        <div
            className="custom-registration"
        >
            <form
                className="custom-form"
                onSubmit={props.handleSubmit}
                // noValidate
            >
                <span className="custom-form-title">
                    Регистрация
                </span>
                <div className="custom-form-input-wrapper">
                    <input
                        type="text"
                        id="name"
                        onChange={props.handleChange}
                        // minLength={2}
                        required
                    />
                    <label
                        htmlFor="name"
                    >
                        Имя
                    </label>
                    {
                        (props.errorData.name.length > 0 && props.formData.name) &&
                        <span className="custom-form-error">
                            {props.errorData.name}
                        </span>
                    }
                </div>
                <div className="custom-form-input-wrapper">
                    <input
                        type="text"
                        id="surname"
                        onChange={props.handleChange}
                        // minLength={2}
                        required
                    />
                    <label
                        htmlFor="surname"
                    >
                        Фамилия
                    </label>
                    {
                        (props.errorData.surname.length > 0 && props.formData.surname) &&
                        <span className="custom-form-error">
                            {props.errorData.surname}
                        </span>
                    }
                </div>
                <div className="custom-form-input-wrapper">
                    <input
                        type="text"
                        id="student_card"
                        onChange={props.handleChange}
                        // minLength={7}
                        maxLength={7}
                        required
                    />
                    <label
                        htmlFor="student_card"
                    >
                        Номер студенческого билета
                    </label>
                    {
                        (props.errorData.student_card.length > 0 && props.formData.student_card) &&
                        <span className="custom-form-error">
                            {props.errorData.student_card}
                        </span>
                    }
                </div>
                <div className="custom-form-input-wrapper">
                    <input
                        type="text"
                        id="email"
                        onChange={props.handleChange}
                        required
                    />
                    <label
                        htmlFor="email"
                    >
                        E-mail
                    </label>
                    {
                        (props.errorData.email.length > 0 && props.formData.email) &&
                        <span className="custom-form-error">
                            {props.errorData.email}
                        </span>
                    }
                </div>
                <div className="custom-form-input-wrapper">
                    <input
                        type="password"
                        id="password"
                        onChange={props.handleChange}
                        required
                    />
                    <label
                        htmlFor="password"
                    >
                        Пароль
                    </label>
                    {
                        (props.errorData.password.length > 0 && props.formData.password) &&
                        <span className="custom-form-error">
                            {props.errorData.password}
                        </span>
                    }
                </div>
                <div className="custom-form-input-wrapper">
                    <input
                        type="password"
                        id="password_confirmation"
                        onChange={props.handleChange}
                        required
                    />
                    <label
                        htmlFor="password_confirmation"
                    >
                        Подтвердите пароль
                    </label>
                    {
                        (props.errorData.password_confirmation.length > 0 && props.formData.password_confirmation) &&
                        <span className="custom-form-error">
                            {props.errorData.password_confirmation}
                        </span>
                    }
                </div>

                    <button
                        className="custom-button"
                        type="submit"
                    >
                        Зарегистрироваться
                    </button>

                    <hr />

                    <Link
                        to="/"
                        className="custom-link"
                    >
                        Войти
                    </Link>
            </form>
        </div>
    );
}
