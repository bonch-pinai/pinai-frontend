import React, { useEffect, useState } from "react";
import RegistrationForm from "./registration-component";
import API from "../../api/api";
import validation from "../../common/scripts/validation";

declare global {
    interface Window {
        grecaptcha: any;
    }
}

interface IRegistrationFormData {
    name: string;
    surname: string;
    student_card: string;
    email: string;
    password: string;
    password_confirmation: string;
}

export default function  Registration() {

    const initData = {
        name: "",
        surname: "",
        student_card: "",
        email: "",
        password: "",
        password_confirmation: ""
    }

    const [formData, setFormData] = useState<IRegistrationFormData>(initData);
    const [formErrorData, setFormErrorData] = useState<IRegistrationFormData>(initData);

    useEffect(() => {
        const loadScriptByURL = (id: string, url: string, callback: () => any) => {
            const isScriptExist = document.getElementById(id);

            if (!isScriptExist) {
                const script = document.createElement("script");
                script.type = "text/javascript";
                script.src = url;
                script.id = id;
                script.onload = () => {
                    if (callback) callback();
                };
                document.body.appendChild(script);
            }

            if (isScriptExist && callback) callback();
        }

        // const key = process.env.REACT_APP_SITE_KEY;

        loadScriptByURL("recaptcha-key", `https://www.google.com/recaptcha/api.js?render=${process.env.REACT_APP_SITE_KEY}`, () => {
            console.log("Script loaded!");
        });
    }, []);

    function handleChange(event: React.ChangeEvent<HTMLInputElement>): void {
        setFormData({...formData, [event.target.id]: event.target.value});
        validation(formData, formErrorData, event.target.id, event.target.value, setFormErrorData);
    }

    function handleSubmit(event: React.FormEvent<HTMLFormElement>) {
        event.preventDefault();

        console.log(formData);
        console.log(formErrorData);
        const checkForm = Object.values(formErrorData).every(value => value === "");
        console.log(checkForm);

        if (checkForm) {

            // const key = process.env.SITE_KEY;
            window.grecaptcha.ready(() => {
                window.grecaptcha.execute(process.env.REACT_APP_SITE_KEY, {action: "register"})
                    .then((token: any) => {
                        API.post(
                            "registration",
                            {...formData, "recaptcha_v3": token}
                        ).catch(
                            (error) => {
                                console.log(error);
                            }
                        )
                    }
                );
            });
        }
    }

    return (
        <RegistrationForm
            formData={formData}
            errorData={formErrorData}
            handleSubmit={handleSubmit}
            handleChange={handleChange}
        />
    );

}

