import React, { useState, useEffect } from "react";
import API from "../../api/api";
import URLQuery from "../../common/scripts/url";
import ResetForm from "./reset-component/reset-form";
import validation from "../../common/scripts/validation";
import ResetDone from "./reset-component/reset-done";
// import useDebounce from "src/common/scripts/useDebounceHook";

interface IResetFormData {
    password: string;
    password_confirmation: string;
}

export default function Reset() {

    const initData = {
        password: "",
        password_confirmation: ""
    }

    const [formData, setFormData] = useState<IResetFormData>(initData);
    const [formErrorData, setFormErrorData] = useState<IResetFormData>(initData);
    const [showError, setShowError] = useState(false);
    const [query, setQuery] = useState<any>();
    const [isReset, setIsReset] = useState(false);

    useEffect(() => {
        const currentLocation = window.location.href;
        const queryURL = URLQuery.getParam(currentLocation, ["email", "token"]);
        setQuery(queryURL);
        setShowError(false);

        // return () => {
        //     cleanup
        // }
    }, [formData]);
    // const [isReset, setIsReset] = useState<boolean>(false);

    // const timerHandler = useRef<number>();

    // useEffect(() => {

    //     const timeHandler = setTimeout(() => {
    //         setFormData({...formData, [target.id]: target.value});
    //         validation(formData, formErrorData, event.target.id, event.target.value, setFormErrorData);
    //     }, 1000)
    //     return () => {
    //         cleanup
    //     }
    // }, [input])

    // tslint:disable-next-line:ban-types

    // tslint:disable-next-line:prefer-const
    // let timeoutID: any;

    // function debounce(func: (...args: any[]) => any, delay: number) {
    //     // let timer: NodeJS.Timeout;

    //     let timeoutID: any;

    //     return function retF(...args: any) {
    //         clearTimeout(timeoutID);
    //         timeoutID = setTimeout(() => {
    //             func(args);
    //         }, delay);
    //     };

    //     clearTimeout(timeoutID);
    //     timeoutID = setTimeout(() => {
    //         func();
    //     }, delay);

    // }

    // const log = () => console.log(formData);

    // tslint:disable-next-line:max-line-length
    // const handleChange = (event: React.ChangeEvent<HTMLInputElement>) => {

    //     // timeout();
    // }

    function handleChange(event: React.ChangeEvent<HTMLInputElement>): void {

        const target = event.target;

        // console.log(formData);
        // clearTimeout(timerHandler.current);

        // console.log(formData);

        // const debounceValue = useDebounce(setFormData({...formData, [target.id]: target.value}), target.id, 1000);

        setFormData({...formData, [target.id]: target.value});

        // debounce(validation(formData, formErrorData, event.target.id, event.target.value, setFormErrorData), 200000);
        // debounce(setFormData({...formData, [target.id]: target.value}), 2000);


        // console.log(formErrorData);

        // const validate = debounce(() => {
        //     setFormData({...formData, [target.id]: target.value});
        // }, 2000);

        // validate();
        // console.Console

        // tslint:disable-next-line:no-shadowed-variable
        // const validate = debounce(() => {
        //     validation(formData, formErrorData, event.target.id, event.target.value, setFormErrorData);
        // }, 1000);

        // validate();

        validation(formData, formErrorData, event.target.id, event.target.value, setFormErrorData);
    }

    function handleSubmit(event: React.FormEvent<HTMLFormElement>): void {
        event.preventDefault();

        const checkForm = Object.values(formErrorData).every(value => value === "");
        console.log(checkForm);
        if (checkForm) {
            console.log(query);

            const resetData = {...formData, ...query};
            API.post(
                "password/reset",
                resetData
            ).then(
                (result) => {
                    console.log(result);
                    setIsReset(true);
                }
            ).catch(
                (error) => {
                    console.log(error);
                }
            );
        } else {
            setShowError(true);
        }
    }

    if (!isReset) {

        return (
            <ResetForm
                formData={formData}
                formErrorData={formErrorData}
                showError={showError}
                handleChange={handleChange}
                handleSubmit={handleSubmit}
            />
        );
    } else {

        return (
            <ResetDone
                formData={query.email}
            />
        )
    }
}