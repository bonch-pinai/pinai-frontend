import React from "react";
import { Link } from "react-router-dom";

interface Props {
    formData: string;
}

export default function ResetDone (props: Readonly<Props>) {

    return (

        <div
            className="custom-reset"
        >
            <form
                className="custom-form"
            >
                <span className="custom-form-title">
                    Сброс пароля
                </span>
                <div className="custom-form-info-done">
                    Новый пароль для аккаунта
                    <span> {props.formData}</span> был успешно установлен
                </div>
                <hr />
                <Link
                    to=""
                    className="custom-link"
                >
                    Вернуться ко входу
                </Link>
            </form>
        </div>
    )
}
