import React from "react";
import { Link } from "react-router-dom";

interface IResetFormData {
    password: string;
    password_confirmation: string;
}

interface Props {
    formErrorData: IResetFormData;
    formData: IResetFormData;
    showError: boolean;
    handleSubmit: (event: any) => any;
    handleChange: (event: React.ChangeEvent<HTMLInputElement>) => any;
}

export default function ResetForm (props: Readonly<Props>) {

    return (

        <div
            className="custom-reset"
        >
            <form
                className="custom-form"
                onSubmit={props.handleSubmit}
            >
                <span className="custom-form-title">
                    Сброс пароля
                </span>
                <div
                    className="custom-form-input-wrapper"
                >
                    <input
                        type="password"
                        id="password"
                        onChange={props.handleChange}
                        value={props.formData.password}
                        required
                        autoFocus
                    />
                    <label
                        htmlFor="password"
                    >
                        * Придумайте новый пароль
                    </label>
                    {
                        (props.formData.password && props.formErrorData.password && props.showError) &&
                        <span className="custom-form-error">
                            {props.formErrorData.password}
                        </span>
                    }
                    {
                        (!props.showError) &&
                        <span className="custom-form-input-info">
                            Латинские символы, мин. 8 знаков, 1 буква и цифра
                        </span>
                    }
                    {
                        (props.formData.password && !props.formErrorData.password && props.showError) &&
                        <span className="custom-form-valid">
                            Пароль соответствует требованиям
                        </span>
                    }
                </div>
                <div
                    className="custom-form-input-wrapper"
                >
                    <input
                        type="password"
                        id="password_confirmation"
                        onChange={props.handleChange}
                        required
                    />
                    <label
                        htmlFor="password_confirmation"
                    >
                        * Повторите пароль
                    </label>
                    {
                        (props.formData.password_confirmation &&
                        props.formErrorData.password_confirmation && props.showError) &&
                        <span className="custom-form-error">
                            {props.formErrorData.password_confirmation}
                        </span>
                    }
                    {
                        (!props.showError) &&
                        <span className="custom-form-input-info">
                            Должно совпадать с паролем
                        </span>
                    }
                </div>
                <button
                        className="custom-button"
                    type="submit"
                >
                    Далее
                </button>
                <hr />
                <Link
                    to=""
                    className="custom-link"
                >
                    Назад
                </Link>
            </form>
        </div>
    )
}
