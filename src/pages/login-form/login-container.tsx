import React, { useState, useEffect } from "react";
import LoginForm from "./login-component";
import API from "../../api/api";
import validation from "../../common/scripts/validation";
import { useHistory } from "react-router-dom";
import useDebounce from "src/common/scripts/useDebounceHook";

interface ILogInFormData {
    email: string;
    password: string;
    remember: boolean | string;
}

export default function Login() {

    const initData = {
        email: "",
        password: "",
        remember: false
    }

    const initErrorData = {...initData, remember: ""};

    const [formData, setFormData] = useState<ILogInFormData>(initData);
    const [formErrorData, setFormErrorData] = useState<ILogInFormData>(initErrorData);
    const debouncedErrors = useDebounce(formErrorData, 500);
    const history = useHistory();

    useEffect(() => {

        function checkIsAuthorized() {

            API.get(
                "check"
            ).then(
                (response: any) => {
                    const data = response.data
                    if (data.alert && data.email) {
                        const email = data.email;
                        console.log("Перенаправляю на email-resend-confirm");
                        history.push({
                            pathname: "/email-resend-confirm",
                            state: {email}
                        });
                    } else {
                        console.log("Перенаправляю на user");
                        history.push("/user");
                    }
                }
            ).catch (
                (error) => {
                    console.log(error);
                    console.log("Дядь, ты не аутентифицирован");
                    // if (error.status === 403) {
                    //     history.push({
                    //         pathname: "/email-resend-confirm",
                    //         state: {email}
                    //     });
                    //     // setIsLogged(true);
                    // } else {
                    //     localStorage.removeItem("email");
                    // }
                }
            );
        }

        checkIsAuthorized();

    }, []); // eslint-disable-line react-hooks/exhaustive-deps

    function handleChange(event: React.ChangeEvent<HTMLInputElement>): void {
        const target = event.target;
        const value = target.type === "checkbox" ? target.checked : target.value;
        setFormData({...formData, [event.target.id]: value});
        validation(formData, formErrorData, event.target.id, event.target.value, setFormErrorData);
    }

    function handleSubmit(event: React.FormEvent<HTMLFormElement>): void {
        event.preventDefault();

        const checkForm = Object.values(formErrorData).every(value => value === "");
        if (checkForm) {
            API.post(
                "login",
                formData
            ).then(
                () => {
                    redirectToUserPage();
                }
            ).catch(
                (error) => {
                    console.log(error);
                    if (error.response.status === 401) {
                        setFormErrorData({...formErrorData, "password": "Проверьте правильность написания почты и пароля"});
                    }
                }
            )
        }
    }

    function redirectToUserPage() {

        API.get(
            "user"
            ).then(
                (result) => {
                    const response = result.data.data;
                    console.log(response);
                    const id = response.user_id;
                    history.push(`/user/${id}/dashboards`);
                }
            ).catch(
                (error) => {
                    if (error.response.status === 403) {
                        const email = formData.email;
                        history.push({
                            pathname: "/email-resend-confirm",
                            state: {email}
                        });
                    }
                }
            )
    }

    return (
        <LoginForm
            formData={formData}
            errorData={debouncedErrors}
            handleSubmit={handleSubmit}
            handleChange={handleChange}
        />
    );
}