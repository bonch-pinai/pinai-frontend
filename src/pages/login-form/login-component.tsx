import React from "react";
import { Link } from "react-router-dom";

interface ILogInFormData {
    email: string;
    password: string;
}

interface Props {
    formData: ILogInFormData;
    errorData: ILogInFormData;

    handleSubmit: (event: any) => any;
    handleChange: (event: React.ChangeEvent<HTMLInputElement>) => any;
}
export default function LoginForm (props: Readonly<Props>) {

    return (

        <div
            className="custom-login"
        >
            <form
                className="custom-form"
                onSubmit={props.handleSubmit}
                // noValidate
            >
                <span className="custom-form-title">
                    Вход
                </span>
                <div className="custom-form-input-wrapper">
                    <input
                        type="text"
                        id="email"
                        onChange={props.handleChange}
                        required
                    />
                    <label
                        htmlFor="email"
                    >
                        E-mail
                    </label>
                    {
                        (props.errorData && props.errorData.email && props.formData.email) &&
                        <span className="custom-form-error">
                            {props.errorData.email}
                        </span>
                    }
                </div>
                <div className="custom-form-input-wrapper">
                    <input
                        type="password"
                        id="password"
                        onChange={props.handleChange}
                        required
                    />
                    <label
                        htmlFor="password"
                    >
                        Пароль
                    </label>
                    {
                        (props.errorData && props.errorData.password && props.formData.password) &&
                        <span className="custom-form-error">
                            {props.errorData.password}
                        </span>
                    }
                </div>
                <div className="custom-form-checkbox-wrapper">
                    <input
                        type="checkbox"
                        id="remember"
                        onChange={props.handleChange}
                    />
                    <label
                        htmlFor="remember"
                    >
                        Запомнить меня
                    </label>
                </div>
                <button
                    className="custom-button"
                    type="submit"
                >
                    Войти
                </button>
                <hr />
                <div
                    className="custom-form-links"
                >
                    <Link
                        to="/forgot"
                        className="custom-link"
                    >
                        Забыл пароль
                    </Link>
                    <Link
                        to="/signup"
                        className="custom-link"
                    >
                        Создать аккаунт
                    </Link>
                </div>
            </form>
        </div>
    );
}