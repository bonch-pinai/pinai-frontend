import React from "react";
import DashboardModule from "./modules/dashboard";

interface IProps {
    dashboards: any;
    isLoaded: boolean;
    redirectToDashboard: (id: any) => any;
}

export default function UserDashboardsComponent(props: Readonly<IProps>) {

    return (
        <div
            className="custom-user-team"
        >
            <div
                className="custom-user-team-wrapper"
            >
                <div
                    className="custom-user-team-topbar"
                >
                    <div
                        className="custom-user-team-topbar-title"
                    >
                        Ваши доски
                    </div>
                </div>
                <div
                    className="custom-user-dashboards"
                >
                    {
                        props.isLoaded &&
                        props.dashboards.map((
                            dashboard: {
                                dashboard_id: number;
                                name: string;
                            },
                            index: number) => {

                            return (
                                <div
                                    key={index}
                                    className="custom-dashboard-module"
                                >
                                    <DashboardModule
                                        dashboard_id={dashboard.dashboard_id}
                                        name={dashboard.name}
                                        redirectToDashboard={props.redirectToDashboard}
                                    />
                                </div>
                            );
                        })
                    }
                </div>
            </div>
        </div>
    )
}