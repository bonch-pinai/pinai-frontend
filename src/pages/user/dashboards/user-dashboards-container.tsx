import React, { useEffect, useState } from "react";
import { useHistory } from "react-router-dom";
import API from "src/api/api";
import UserDashboardsComponent from "./user-dashboards-components";

export default function UserDashboards() {

    const [dashboards, setDashboards] = useState();
    const [isLoaded, setIsLoaded] = useState(false);
    const history = useHistory();

    useEffect(() => {

        API.get(
            "dashboard"
        ).then(
            (response) => {
                const data = response.data.data;
                setDashboards(data);
                console.log(dashboards);
                setIsLoaded(true);
            })
    }, []); // eslint-disable-line react-hooks/exhaustive-deps

    function redirectToDashboard(value: number) {
        history.push(`/user/dashboard/${value}`);
    }

    return (

        <UserDashboardsComponent
            dashboards={dashboards}
            isLoaded={isLoaded}
            redirectToDashboard={redirectToDashboard}
        />
    );
}