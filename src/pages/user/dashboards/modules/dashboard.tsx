import React from "react";

interface IProps {
    dashboard_id: number;
    name: string;

    redirectToDashboard: (event: any) => any;
}

export default function DashboardModule(props: Readonly<IProps>) {

    function redirectToDashboard() {
        props.redirectToDashboard(props.dashboard_id);
    }

    return (
        <div
            className="custom-dashboard-module-wrapper"
            onClick={redirectToDashboard}
        >
            <div
                className="custom-dashboard-module-name"
            >
                {props.name}
            </div>
            <hr />
        </div>
    )
}