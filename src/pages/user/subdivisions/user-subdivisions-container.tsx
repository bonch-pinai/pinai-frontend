import React, { useEffect, useState } from "react";
import { useRouteMatch } from "react-router-dom";
import API from "src/api/api";
import CreateSubdivisionWindow from "./subdivision-create";
import UserSubdivisionsComponent from "./user-subdivisions-component";

interface IProps {
    isManager: boolean;
    id: number;
}

export default function UserSubdivisions(props: Readonly<IProps>) {

    const [teams, setTeams] = useState<any>();
    const [isLoaded, setIsLoaded] = useState(false);
    const [isCreateWindowVisible, setIsCreateWindowVisible] = useState(false);

    useEffect(() => {

        API.get(
            "subdivision"
        ).then(
            (response) => {
                const subdivisions = response.data.data;
                setTeams(subdivisions)
                console.log(subdivisions);
                setIsLoaded(true);
            })
    }, []);

    const { path } = useRouteMatch();

    function handleCreateWindowClose() {
        setIsCreateWindowVisible(false);
    }

    function handleCreateWindowShow() {
        setIsCreateWindowVisible(true);
    }

    function handleCreateTeam(newTeam: any) {
        const newArray = teams.concat(newTeam);
        setTeams(newArray);
    }

    return (

        <>
            <UserSubdivisionsComponent
                teams={teams}
                path={path}
                isLoaded={isLoaded}
                isManager={props.isManager}
                handleOpenCreateWindow={handleCreateWindowShow}
            />
            {
                isCreateWindowVisible &&
                <CreateSubdivisionWindow
                    id={props.id}
                    handleClose={handleCreateWindowClose}
                    createTeam={handleCreateTeam}
                />
            }
        </>
    );
}
