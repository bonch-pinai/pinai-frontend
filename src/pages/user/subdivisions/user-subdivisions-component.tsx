import React from "react";
import Team from "./team";

interface IProps {
    path: string;
    teams: any;
    isLoaded: boolean;
    isManager: boolean;
    handleOpenCreateWindow: (event: any) => any;
}

export default function UserSubdivisionsComponent(props: Readonly<IProps>) {

    return (
        <div
            className="custom-user-subdivisions"
        >
            <div
                className="custom-user-subdivisions-wrapper"
            >
                <div
                    className="custom-user-subdivisions-topbar"
                >
                    <div
                        className="custom-user-subdivisions-topbar-title"
                    >
                        Ваши подразделения
                    </div>
                    {
                        props.isManager &&
                        <div
                            className= "custom-icons-plus"
                            onClick={props.handleOpenCreateWindow}
                        />
                    }
                </div>
                <div
                    className="custom-user-subdivisions-grid"
                >
                    {
                        props.isLoaded &&
                        props.teams.map((subdivision: { subdivision_id: number; name: string; }, index: number) => {

                            return (
                                <div
                                    key={index}
                                    className="custom-user-subdivisions-block"
                                >
                                    <Team
                                        id={subdivision.subdivision_id}
                                        name={subdivision.name}
                                        path={props.path}
                                    />
                                </div>
                            );
                        })
                    }
                </div>
            </div>
        </div>
    )
}
