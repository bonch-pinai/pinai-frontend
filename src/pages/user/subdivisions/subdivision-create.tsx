import React, { useEffect, useRef, useState } from "react";
import API from "src/api/api";

interface IProps {
    id: number;

    handleClose: () => any;
    createTeam: (team: any) => any;
}

export default function CreateSubdivisionWindow(props: Readonly<IProps>) {

    const [name, setName] = useState("");
    const ref = useRef<HTMLFormElement>(null);

    useEffect(() => {
        document.addEventListener("mousedown", handleClose);

        return () => {
            document.removeEventListener("mousedown", handleClose);
          };
    }, [ref]); // eslint-disable-line react-hooks/exhaustive-deps

    function handleClose(event: any) {
        if (ref.current && !ref.current.contains(event.target)) {
            props.handleClose();
        }
    }

    function handleChange(event: any) {
        setName(event.target.value);
    }

    function handleSubmit(event: any) {
        event.preventDefault();

        const payload = {
            leader_id: props.id,
            name: name
        };

        API.post(
            "subdivision",
            payload
        ).then(
            (response) => {
                const data = response.data.data;
                console.log(data);
                props.createTeam({
                    subdivision_id: data.subdivision_id,
                    name: data.name
                });
                props.handleClose();
            }
        ).catch(
            (error) => {
                console.log(error);
            }
        )
    }

    return (
        <div
            className="custom-modal-window"
        >
            <form
                className="custom-form"
                onSubmit={handleSubmit}
                ref={ref}
            >
                <span className="custom-form-title">
                    Создание подразделения
                </span>
                <div className="custom-form-input-wrapper">
                    <input
                        type="text"
                        id="name"
                        onChange={handleChange}
                        autoFocus
                        required
                    />
                    <label
                        htmlFor="name"
                    >
                        Название
                    </label>
                </div>
                <button
                    className="custom-button"
                    type="submit"
                >
                    Создать
                </button>
            </form>
        </div>
    )
}
