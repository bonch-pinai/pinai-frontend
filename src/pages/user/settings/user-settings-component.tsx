import React from "react";
// import DropdownMenu from "src/common/modules/dropdown-menu/dropdown-menu-container";

interface IUserData {
    user_id: number;
    student_card: string;
    email: string;
    password?: string;
    new_password?: string;
    new_password_confirmation?: string;
    name: string;
    surname: string;
    avatar: string;
    contacts: {
        inst: string;
        telegram: string;
        vk: string;
    };
    settings: {
        deadline: boolean;
        notification: boolean;
    }
}

interface IFormError {
    student_card: string;
    email: string;
    password: string;
    new_password: string;
    new_password_confirmation: string;
    name: string;
    surname: string;
}

interface IPassword {
    password: string;
    new_password: string;
    new_password_confirmation: string;
}

interface IProps {
    userData: IUserData;
    formData: IUserData;
    errorData: IFormError;
    passwordData: IPassword;
    showError: boolean;
    showSaveButtons: {
        avatar: boolean,
        names: boolean,
        student_card: boolean,
        email: boolean,
        password: boolean,
        settings: boolean,
        contacts: boolean
    };

    setPhoto: (event: any) => any;
    uploadPhoto: () => any;
    handleFormChange: (event: any) => any;
    handlePasswordChange: (event: any) => any;
    handleSubmitNames: (event: any) => any;
    handleSubmitID: (event: any) => any;
    handleSubmitEmail: (event: any) => any;
    handleSubmitPassword: (event: any) => any;
}

// tslint:disable-next-line:cyclomatic-complexity
export default function UserSettingsComponent(props: Readonly<IProps>) {

    return (
        <div
            className="custom-user-settings"
        >
            <div
                className="custom-user-settings-wrapper"
            >
                <div
                    className="custom-user-settings-topbar"
                >
                    <div
                        className="custom-user-settings-topbar-title"
                    >
                        Настройки пользователя
                    </div>
                    {/* <DropdownMenu
                        userData={props.userData}
                    /> */}
                </div>
                <div
                    className="custom-user-settings-body"
                >
                    <div
                        className="custom-user-settings-body-main"
                    >
                        <span
                            className="custom-user-settings-body-main-title"
                        >
                            Профиль
                        </span>
                        {/* <div
                            className="custom-user-settings-body-col"
                        > */}
                            <div
                                className="custom-user-settings-body-main-avatar"
                            >
                                <div
                                    className="custom-user-settings-body-main-avatar-overlay"
                                >
                                    <div
                                        className="custom-user-settings-body-main-avatar-img"
                                        style = {{ backgroundImage: `url(${props.formData.avatar})` }}
                                    />
                                    {/* <div
                                        className="custom-user-settings-body-main-avatar-upload"
                                    >
                                        <input
                                            type="file"
                                            id="photo"
                                            onChange={props.setPhoto}
                                            accept="image/png, image/jpeg"
                                        />
                                        <label
                                            htmlFor="photo"
                                        >
                                            Загрузить фото
                                        </label>
                                    </div> */}
                                    <div
                                        className="custom-user-settings-body-main-avatar-cross"
                                        // onClick={props.uploadPhoto}
                                    />
                                    {
                                        !props.showSaveButtons.avatar &&
                                        <div
                                            className="custom-user-settings-body-main-avatar-upload"
                                        >
                                            <input
                                                type="file"
                                                id="photo"
                                                onChange={props.setPhoto}
                                                accept="image/png, image/jpeg"
                                            />
                                            <label
                                                htmlFor="photo"
                                            >
                                                <svg
                                                    viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg">
                                                    <path d="M0 0h24v24H0z" fill="none" />
                                                    <circle cx="12" cy="12" r="3.2" />
                                                    <path d="M9 2L7.17 4H4c-1.1 0-2 .9-2 2v12c0 1.1.9 2 2 2h16c1.1 0 2-.9
                                                    2-2V6c0-1.1-.9-2-2-2h-3.17L15
                                                    2H9zm3 15c-2.76 0-5-2.24-5-5s2.24-5
                                                    5-5 5 2.24 5 5-2.24 5-5 5z" />
                                                </svg>
                                            </label>
                                        </div>
                                    }
                                    {
                                        props.showSaveButtons.avatar &&
                                        <div
                                            className="custom-user-settings-body-main-avatar-check"
                                            onClick={props.uploadPhoto}
                                        />
                                    }
                                </div>
                            </div>
                            <form
                                className="custom-form-settings"
                                onSubmit={props.handleSubmitNames}
                            >
                                <div className="custom-form-input-wrapper">
                                    <input
                                        type="text"
                                        id="name"
                                        onChange={props.handleFormChange}
                                        value={props.formData.name}
                                        required
                                    />
                                    <label
                                        htmlFor="name"
                                    >
                                        Имя
                                    </label>
                                    {
                                        (props.errorData.name.length > 0 && props.formData.name && props.showError) &&
                                        <span className="custom-form-error">
                                            {props.errorData.name}
                                        </span>
                                    }
                                </div>
                                <div className="custom-form-input-wrapper">
                                    <input
                                        type="text"
                                        id="surname"
                                        onChange={props.handleFormChange}
                                        value={props.formData.surname}
                                        required
                                    />
                                    <label
                                        htmlFor="surname"
                                    >
                                        Фамилия
                                    </label>
                                    {
                                        (props.errorData.surname.length > 0 &&
                                            props.formData.surname && props.showError) &&
                                        <span className="custom-form-error">
                                            {props.errorData.surname}
                                        </span>
                                    }
                                </div>
                                {
                                    props.showSaveButtons.names &&
                                    <button
                                        className="custom-button"
                                    >
                                        Сохранить
                                    </button>
                                }
                            </form>
                        {/* </div> */}
                        <hr />
                        <span
                            className="custom-user-settings-body-main-title"
                        >
                            Студенческий билет
                        </span>
                        <form
                            className="custom-form-settings"
                            onSubmit={props.handleSubmitID}
                        >
                            <div className="custom-form-input-wrapper">
                                <input
                                    type="text"
                                    id="student_card"
                                    onChange={props.handleFormChange}
                                    value={props.formData.student_card}
                                    maxLength={7}
                                    required
                                />
                                <label
                                    htmlFor="student_card"
                                >
                                    Номер
                                </label>
                                {
                                    (props.errorData.student_card.length > 0 &&
                                        props.formData.student_card && props.showError) &&
                                    <span className="custom-form-error">
                                        {props.errorData.student_card}
                                    </span>
                                }
                            </div>
                            {
                                props.showSaveButtons.student_card &&
                                <button
                                    className="custom-button"
                                >
                                    Сохранить
                                </button>
                            }
                        </form>
                        <hr />
                        <span
                            className="custom-user-settings-body-main-title"
                        >
                            Почтовый адрес
                        </span>
                        <form
                            className="custom-form-settings"
                            onSubmit={props.handleSubmitEmail}
                        >
                            <div className="custom-form-input-wrapper">
                                <input
                                    type="text"
                                    id="email"
                                    onChange={props.handleFormChange}
                                    value={props.formData.email}
                                    required
                                />
                                <label
                                    htmlFor="email"
                                >
                                    E-mail
                                </label>
                                {
                                    (props.errorData.email.length > 0 && props.formData.email) &&
                                    <span className="custom-form-error">
                                        {props.errorData.email}
                                    </span>
                                }
                                </div>
                            {
                                props.showSaveButtons.email &&
                                <button
                                    className="custom-button"
                                >
                                    Сохранить
                                </button>
                            }
                        </form>
                        <hr />
                        <span
                            className="custom-user-settings-body-main-title"
                        >
                            Пароль
                        </span>
                        <form
                            className="custom-form-settings"
                            onSubmit={props.handleSubmitPassword}
                        >
                            <div className="custom-form-input-wrapper">
                                <input
                                    type="password"
                                    id="password"
                                    onChange={props.handlePasswordChange}
                                    value={props.passwordData.password}
                                    required
                                />
                                <label
                                    htmlFor="password"
                                >
                                    Старый пароль
                                </label>
                                {
                                    (props.errorData.password.length > 0 && props.passwordData.password) &&
                                    props.showError &&
                                    <span className="custom-form-error">
                                        {props.errorData.password}
                                    </span>
                                }
                            </div>
                            <div className="custom-form-input-wrapper">
                                <input
                                    type="password"
                                    id="new_password"
                                    onChange={props.handlePasswordChange}
                                    value={props.passwordData.new_password}
                                    required
                                />
                                <label
                                    htmlFor="new_password"
                                >
                                    Новый пароль
                                </label>
                                {
                                    (props.errorData.new_password.length > 0
                                        && props.passwordData.new_password) && props.showError &&
                                    <span className="custom-form-error">
                                        {props.errorData.new_password}
                                    </span>
                                }
                            </div>
                            <div className="custom-form-input-wrapper">
                                <input
                                    type="password"
                                    id="new_password_confirmation"
                                    onChange={props.handlePasswordChange}
                                    value={props.passwordData.new_password_confirmation}
                                    required
                                />
                                <label
                                    htmlFor="new_password_confirmation"
                                >
                                    Повторите новый пароль
                                </label>
                                {
                                    (props.errorData.new_password_confirmation.length > 0
                                        && props.passwordData.new_password_confirmation) && props.showError &&
                                    <span className="custom-form-error">
                                        {props.errorData.new_password_confirmation}
                                    </span>
                                }
                            </div>
                            {
                                props.showSaveButtons.password &&
                                <button
                                    className="custom-button"
                                >
                                    Сохранить
                                </button>
                            }
                        </form>
                    </div>
                </div>
            </div>
        </div>
    )
}
