import React, { useEffect, useState } from "react";
import UserSettingsComponent from "./user-settings-component";
import validation from "src/common/scripts/validation";
import API from "src/api/api";
import { useHistory } from "react-router-dom";

interface IUserData {
    user_id: number;
    student_card: string;
    email: string;
    name: string;
    surname: string;
    avatar: string;
    contacts: {
        inst: string;
        telegram: string;
        vk: string;
    };
    settings: {
        deadline: boolean;
        notification: boolean;
    }
}

interface IPassword {
    password: string;
    new_password: string;
    new_password_confirmation: string;
}

interface IFormError {
    student_card: string;
    email: string;
    password: string;
    new_password: string;
    new_password_confirmation: string;
    name: string;
    surname: string
}

interface IProps {
    userData: IUserData;
    setUserData: (data: IUserData) => any;
}

export default function UserSettings(props: Readonly<IProps>) {

    const initFormData = {
        user_id: props.userData.user_id,
        student_card: props.userData.student_card,
        email: props.userData.email,
        name: props.userData.name,
        surname: props.userData.surname,
        avatar: props.userData.avatar,
        contacts: {
            inst: props.userData.contacts.inst,
            telegram: props.userData.contacts.telegram,
            vk: props.userData.contacts.vk
        },
        settings: {
            deadline: props.userData.settings.deadline,
            notification: props.userData.settings.notification
        }
    }

    const initErrorData = {
        student_card: "",
        email: "",
        name: "",
        surname: "",
        password: "",
        new_password: "",
        new_password_confirmation: ""
    }

    const initPassword = {
        password: "",
        new_password: "",
        new_password_confirmation: ""
    }

    // useEffect(() => {
    //     setFormData(initFormData);
    // }, [initFormData]);

    const initShowSaveButton = {
        avatar: false,
        names: false,
        student_card: false,
        password: false,
        email: false,
        settings: false,
        contacts: false
    }

    const [file, setFile] = useState<any>();
    const [formData, setFormData] = useState(initFormData);
    const [password, setPassword] = useState<IPassword>(initPassword)
    const [formErrorData, setFormErrorData] = useState<IFormError>(initErrorData);
    const [showError, setShowError] = useState(false);
    const [showSaveButtons, setShowSaveButtons] = useState(initShowSaveButton);
    const history = useHistory();

    useEffect(() => {
        setShowError(false);
    }, [formData]);

    // useEffect(() => {
    //     return () => {
    //         console.log("cleaned up");
    //     };
    // }, []);

    function setPhoto(event: any) {
        const imgFile = event.target.files[0];
        setFile(event.target.files[0]);

        const reader = new FileReader();

        reader.onloadend = () => {
            if (reader.result) {
                setFormData({...formData, avatar: reader.result.toString()});
                props.setUserData({...props.userData, "avatar": reader.result.toString()});
            }
        }

        reader.readAsDataURL(imgFile);
        setShowSaveButtons({...showSaveButtons, "avatar": true});
    }

    function uploadPhoto() {
        const data = new FormData();
        data.append("avatar", file);

        API.post(
            "user/avatar",
            data
        ).then(() => {
            setShowSaveButtons({...showSaveButtons, "avatar": false});
        })
    }

    function handleFormChange(event: any): void {
        const target = event.target;
        const value = target.value;
        const id = event.target.id;
        setFormData({...formData, [id]: value});

        if (id === "name" || id === "surname") {
            setShowSaveButtons({...showSaveButtons, "names": true});
        } else {
            setShowSaveButtons({...showSaveButtons, [id]: true});
        }

        if (target.type !== "checkbox") {
            validation(formData, formErrorData, event.target.id, event.target.value, setFormErrorData);
        }
    }

    function handlePasswordChange(event: any): void {
        const value = event.target.value;
        const id = event.target.id;

        setPassword({...password, [id]: value});
        setShowSaveButtons({...showSaveButtons, "password": true});
        validation(password, formErrorData, event.target.id, event.target.value, setFormErrorData);
    }

    function Update(payload: any, type: string) {

        const route = type === "password" ? "password/replace" : "user";
        const method = type === "password" ? "post" : "put";
        API[method](
            route,
            payload
        ).then(
            (response) => {
                console.log(response);
                if (type === "password") {
                    setPassword({
                        password: "",
                        new_password: "",
                        new_password_confirmation: ""
                    });
                } else if (type === "email") {
                    props.setUserData(formData);
                    const email = formData.email;
                    console.log("Перенаправляю на email-resend-confirm");
                    history.push({
                        pathname: "/email-resend-confirm",
                        state: {email}
                    });
                } else {
                    props.setUserData(formData);
                }

                setShowSaveButtons({...showSaveButtons, [type]: false});
            }
        ).catch(
            (error) => {
                console.log(error);
            }
        );
    }

    function handleSubmitNames(event: React.FormEvent<HTMLFormElement>): void {
        event.preventDefault();

        if (!formErrorData.name && !formErrorData.surname) {
            const payload = {
                name: formData.name,
                surname: formData.surname
            }

            Update(payload, "names");

        } else {
            setShowError(true);
        }
    }

    function handleSubmitID(event: React.FormEvent<HTMLFormElement>): void {
        event.preventDefault();

        if (!formErrorData.student_card) {
            const data = {
                student_card: formData.student_card
            }

            console.log(data);
            Update(data, "student_card");
        } else {
            setShowError(true);
        }
    }

    function handleSubmitEmail(event: React.FormEvent<HTMLFormElement>): void {
        event.preventDefault();

        if (!formErrorData.email) {
            const payload = {
                email: formData.email
            }

            Update(payload, "email");
        } else {
            setShowError(true);
        }
    }

    function handleSubmitPassword(event: React.FormEvent<HTMLFormElement>): void {
        event.preventDefault();

        if (!formErrorData.password &&
            !formErrorData.new_password && !formErrorData.new_password_confirmation) {
            const payload = {
                old_password: password.password,
                new_password: password.new_password,
                new_password_confirmation: password.new_password_confirmation
            }

            Update(payload, "password");
        } else {
            setShowError(true);
        }
    }

    return (

        <UserSettingsComponent
            userData={props.userData}
            formData={formData}
            errorData={formErrorData}
            passwordData={password}
            showError={showError}
            showSaveButtons={showSaveButtons}
            handleFormChange={handleFormChange}
            handlePasswordChange={handlePasswordChange}
            setPhoto={setPhoto}
            uploadPhoto={uploadPhoto}
            handleSubmitNames={handleSubmitNames}
            handleSubmitID={handleSubmitID}
            handleSubmitEmail={handleSubmitEmail}
            handleSubmitPassword={handleSubmitPassword}
        />
    )
}
