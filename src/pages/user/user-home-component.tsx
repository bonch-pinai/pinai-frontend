import React from "react";
import { Switch, Route } from "react-router-dom";
import Sidebar from "src/common/modules/sidebar/sidebar-container";
import DashboardTaskView from "../dashboard-task/dashboard-task-container";
import Dashboard from "../dashboard/dashboard-container";
import Teams from "../team/team-container";
import UserDashboards from "./dashboards/user-dashboards-container";
import UserSettings from "./settings/user-settings-container";
import UserSubdivisions from "./subdivisions/user-subdivisions-container";
import UserTasks from "./tasks/user-tasks-container";

interface IUserData {
    user_id: number;
    student_card: string;
    email: string;
    name: string;
    surname: string;
    avatar: string;
    contacts: {
        inst: string;
        telegram: string;
        vk: string;
    };
    settings: {
        deadline: boolean;
        notification: boolean;
    }
}

interface IProps {
    userData: IUserData;
    path: string;
    isManager: boolean;
    setUserData: (data: IUserData) => any;
}

export default function UserHomeComponent (props: Readonly<IProps>) {

    return (

        <div
            className="custom-user-home"
        >
            <Sidebar
                    userData={props.userData}
                    path={props.path}
            />
            <Route path={"/user/dashboard/:id/task/:taskID"}>
                <DashboardTaskView
                    userData={props.userData}
                />
            </Route>
            <Switch>
                <Route path={`${props.path}/${props.userData.user_id}/dashboards`}>
                    <UserDashboards />
                </Route>
                <Route path={`${props.path}/${props.userData.user_id}/tasks`}>
                    <UserTasks
                        id={props.userData.user_id}
                    />
                </Route>
                <Route path={`${props.path}/${props.userData.user_id}/settings`}>
                    <UserSettings
                        setUserData={props.setUserData}
                        userData={props.userData}
                    />
                </Route>
                <Route exact path={`${props.path}/${props.userData.user_id}/teams`}>
                    <UserSubdivisions
                        id={props.userData.user_id}
                        isManager={props.isManager}
                    />
                </Route>
                <Route path={"/user/team/:id"}>
                    <Teams
                        user_id={props.userData.user_id}
                        isManager={props.isManager}
                    />
                </Route>
                <Route path={"/user/dashboard/:id"}>
                    <Dashboard
                        userID={props.userData.user_id}
                    />
                </Route>
            </Switch>
        </div>
    );
}
