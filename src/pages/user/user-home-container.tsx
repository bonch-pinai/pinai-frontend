import React, { useEffect, useState } from "react";
import UserHomeComponent from "./user-home-component";
import API from "src/api/api";
import { useHistory, useRouteMatch } from "react-router-dom";

interface IUserData {
    user_id: number;
    student_card: string;
    email: string;
    name: string;
    surname: string;
    avatar: string;
    contacts: {
        inst: string;
        telegram: string;
        vk: string;
    };
    settings: {
        deadline: boolean;
        notification: boolean;
    }
}

export default function UserHome() {

    const initData = {
        user_id: 0,
        student_card: "",
        email: "",
        name: "",
        surname: "",
        avatar: "",
        contacts: {
            inst: "",
            telegram: "",
            vk: "",
        },
        settings: {
            deadline: false,
            notification: false
        }
    }

    const [userData, setUserData] = useState<IUserData>(initData);
    const [isManager, setIsManager] = useState(false);
    const history = useHistory();

    useEffect(() => {

        API.get(
            "user"
        ).then(
            (response: any) => {
                const userInfo = response.data.data;
                setUserData(userInfo);
                console.log(userData);

                if (userInfo.manager === true) {
                    setIsManager(true);
                }
            }
        ).catch(
            error => {
                history.push("/");
                console.log(error);
            }
        );
    }, []); // eslint-disable-line react-hooks/exhaustive-deps

    const { path } = useRouteMatch();

    function changeUserData(data: IUserData) {
        setUserData(data);
    }

    return (

        <UserHomeComponent
            setUserData={changeUserData}
            userData={userData}
            path={path}
            isManager={isManager}
        />
    );
}