import React from "react";
import TaskModule from "./modules/task";

interface IProps {
    tasks: any;
    isLoaded: boolean;
}

export default function UserTasksComponent(props: Readonly<IProps>) {

    return (
        <div
            className="custom-user-team"
        >
            <div
                className="custom-user-team-wrapper"
            >
                <div
                    className="custom-user-team-topbar"
                >
                    <div
                        className="custom-user-team-topbar-title"
                    >
                        Задачи
                    </div>
                </div>
                <div
                    className="custom-team-member-body"
                >
                    {
                        props.isLoaded &&
                        props.tasks.map((
                            task: {
                                task_id: number;
                                dashboard_id: number;
                                name: string;
                                state: number;
                                executor_id: number;
                                deadline: number;
                            },
                            index: number) => {

                            return (
                                <div
                                    key={index}
                                    className="custom-team-member"
                                >
                                    <TaskModule
                                        task_id={task.task_id}
                                        dashboard_id={task.dashboard_id}
                                        name={task.name}
                                        state={task.state}
                                        executor_id={task.executor_id}
                                        deadline={task.deadline}
                                    />
                                </div>
                            );
                        })
                    }
                </div>
            </div>
        </div>
    )
}