import React, { useEffect, useState } from "react";
import API from "src/api/api";
import UserTasksComponent from "./user-tasks.component";

interface IProps {
    id: number;
}

export default function UserTasks(props: Readonly<IProps>) {

    const [tasks, setTasks] = useState();
    const [isLoaded, setIsLoaded] = useState(false);

    useEffect(() => {

        API.get(
            "task"
        ).then(
            (response) => {
                const data = response.data.data;
                const createdTasks = data.created_tasks;
                const executedTasks = data.executed_tasks;
                const markedTasks = data.marked_tasks;

                const allTasks = createdTasks.concat(executedTasks, markedTasks);

                setTasks(allTasks)
                console.log(allTasks);
                console.log(props.id)
                setIsLoaded(true);
            })
    }, []); // eslint-disable-line react-hooks/exhaustive-deps

    return (

        <UserTasksComponent
            isLoaded={isLoaded}
            tasks={tasks}
        />
    );
}