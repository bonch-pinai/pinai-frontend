import React from "react";
import { Link } from "react-router-dom";

interface IProps {
    task_id: number;
    dashboard_id: number;
    name: string;
    state: number;
    executor_id: number;
    deadline: number;
}

export default function TaskModule(props: Readonly<IProps>) {

    let cssStyle = "";
    let label = "";
    const date = new Date(props.deadline).toISOString().slice(0, 16).replace(/-/g, "/").replace("T", " ");
    const dateFormatted = `${date.substring(11, 16)} ${date.substring(0, 11)}`;

    switch (props.state) {
        case 0:
            cssStyle = "created";
            label = "В процессе";
            break;

        case 1:
            cssStyle = "marked";
            label = "Проверена";
            break;

        case 2:
            cssStyle = "executed"
            label = "Выполнена";
            break;

        default:
            break;
    }

    return (
        <div
            className="custom-team-member-container"
        >
            <div
                className={`custom-tasks-status-${cssStyle}`}
            >
                {label}
            </div>
            <div
                className="custom-team-member-name"
            >
                <Link
                    to={`/user/dashboard/${props.dashboard_id}/task/${props.task_id}`}
                    className="custom-tasks-link"
                >
                    {props.name}
                </Link>
            </div>
            <div
                className="custom-tasks-deadline"
            >
                {dateFormatted}
            </div>
        </div>
    )
}