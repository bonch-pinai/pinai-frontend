import React from "react";
import { Route, Switch } from "react-router-dom";
import TeamDashboards from "./pages/team-dashboards";
import TeamSettings from "./pages/team-settings";

interface IProps {
    user_id: number;
    subdivisionData: any;
    isLoaded: boolean;
    isLeader: boolean;
    isManager: boolean;
    members: any;
    formTeamName: string;

    handleOpenModalAddMember: () => any;
    handleOpenModalCreateDashboard: () => any;
    handleDeleteMember: (id: number) => any;
    handleFormTeamNameChange: (event: any) => any;
    handleFormTeamNameSubmit: (event: any) => any;
    handleLeaderChange: (id: any) => any;
    redirectToDashboard: (id: any) => any;
}

export default function TeamComponent(props: Readonly<IProps>) {

    return (

        <Switch>
            <Route path={"/user/team/:id/settings"} >
                <TeamSettings
                    user_id={props.user_id}
                    subdivisionData={props.subdivisionData}
                    members={props.members}
                    isLeader={props.isLeader}
                    isManager={props.isManager}
                    isLoaded={props.isLoaded}
                    teamName={props.formTeamName}
                    handleOpenModal={props.handleOpenModalAddMember}
                    handleDeleteMember={props.handleDeleteMember}
                    handleFormTeamNameChange={props.handleFormTeamNameChange}
                    handleFormTeamNameSubmit={props.handleFormTeamNameSubmit}
                    handleLeaderChange={props.handleLeaderChange}
                />
            </Route>
            <Route path={"/user/team/:id"} >
                <TeamDashboards
                    user_id={props.user_id}
                    subdivisionData={props.subdivisionData}
                    dashboards={props.subdivisionData.dashboards}
                    members={props.members}
                    isLeader={props.isLeader}
                    isManager={props.isManager}
                    isLoaded={props.isLoaded}
                    handleOpenModal={props.handleOpenModalCreateDashboard}
                    handleDeleteMember={props.handleDeleteMember}
                    handleLeaderChange={props.handleLeaderChange}
                    redirectToDashboard={props.redirectToDashboard}
                />
            </Route>
        </Switch>
    )
}
