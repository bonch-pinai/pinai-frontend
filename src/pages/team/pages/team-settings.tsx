import React from "react";
import MemberModule from "../modules/member";

interface IProps {
    user_id: number;
    subdivisionData: any;
    isLoaded: boolean;
    isLeader: boolean;
    isManager: boolean;
    members: any;
    teamName: string;
    handleOpenModal: () => any;
    handleDeleteMember: (id: number) => any;
    handleFormTeamNameChange: (event: any) => any;
    handleFormTeamNameSubmit: (event: any) => any;
    handleLeaderChange: (id: any) => any;
}

export default function TeamSettings(props: Readonly<IProps>) {

    return (
        <div
            className="custom-user-team"
        >
            <div
                className="custom-user-team-wrapper"
            >
                <div
                    className="custom-user-team-topbar"
                >
                    <div
                        className="custom-user-team-topbar-title"
                    >
                        {props.subdivisionData.name}
                    </div>
                </div>
                {
                    props.isLeader &&
                    <>
                        <div
                            className="custom-user-team-container-title"
                        >
                            Настройки
                        </div>
                        <div
                            className="custom-user-settings-body"
                        >
                            <div
                                className="custom-user-settings-body-main"
                            >
                                <form
                                    className="custom-form-settings"
                                    onSubmit={props.handleFormTeamNameSubmit}
                                >
                                    <div className="custom-form-input-wrapper">
                                        <input
                                            type="text"
                                            id="name"
                                            onChange={props.handleFormTeamNameChange}
                                            value={props.teamName}
                                            required
                                        />
                                        <label
                                            htmlFor="name"
                                        >
                                            Название
                                        </label>
                                    </div>
                                    {
                                        // props.showSaveButtons.names &&
                                        <button
                                            className="custom-button"
                                        >
                                            Сохранить
                                        </button>
                                    }
                                </form>
                            </div>
                        </div>
                    </>
                }
                <div
                    className="custom-user-team-container-title"
                >
                    Участники
                    {
                        props.isLoaded &&
                        props.isLeader &&
                        <div
                            className= "custom-icons-plus"
                            onClick={props.handleOpenModal}
                        />
                    }
                </div>
                <div
                    className="custom-team-member-body"
                >
                    {
                        props.isLoaded &&
                        props.members.map((
                            member: {
                                avatar: string;
                                full_name: string;
                                user_id: number
                            },
                            index: number) => {

                            return (
                                <div
                                    key={index}
                                    className="custom-team-member"
                                >
                                    <MemberModule
                                        name={member.full_name}
                                        avatar={member.avatar}
                                        memberID={member.user_id}
                                        userID={props.user_id}
                                        isManager={props.isManager}
                                        isLeader={props.isLeader}
                                        delete={true}
                                        handleMemberStatusChange={props.handleDeleteMember}
                                        handleLeaderChange={props.handleLeaderChange}
                                    />
                                </div>
                            );
                        })
                    }
                </div>
            </div>
        </div>
    )
}
