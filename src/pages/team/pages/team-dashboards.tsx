import React from "react";
import DashboardModule from "src/pages/user/dashboards/modules/dashboard";

interface IProps {
    user_id: number;
    subdivisionData: any;
    dashboards: any;
    isLoaded: boolean;
    isLeader: boolean;
    isManager: boolean;
    members: any;
    handleOpenModal: () => any;
    handleDeleteMember: (id: number) => any;
    handleLeaderChange: (id: any) => any;
    redirectToDashboard: (id: any) => any;
}

export default function TeamDashboards(props: Readonly<IProps>) {

    return (
        <div
            className="custom-user-team"
        >
            <div
                className="custom-user-team-wrapper"
            >
                <div
                    className="custom-user-team-topbar"
                >
                    <div
                        className="custom-user-team-topbar-title"
                    >
                        Доски команды {props.subdivisionData.name}
                    </div>
                    {
                        props.isLoaded &&
                        <div
                            className= "custom-icons-plus"
                            onClick={props.handleOpenModal}
                        />
                    }
                </div>
                <div
                    className="custom-user-dashboards"
                >
                    {
                        props.isLoaded &&
                        props.dashboards.map((
                            dashboard: {
                                dashboard_id: number;
                                name: string;
                            },
                            index: number) => {

                            return (
                                <div
                                    key={index}
                                    className="custom-dashboard-module"
                                >
                                    <DashboardModule
                                        dashboard_id={dashboard.dashboard_id}
                                        name={dashboard.name}
                                        redirectToDashboard={props.redirectToDashboard}
                                    />
                                </div>
                            );
                        })
                    }
                </div>
            </div>
        </div>
    )
}
