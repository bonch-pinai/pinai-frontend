import React from "react";

interface IProps {
    name: string;
    avatar: string;
    userID: number;
    memberID: number;
    isLeader: boolean;
    isCreator?: boolean;
    isManager?: boolean;

    additionalClass?: string;
    delete: boolean;

    handleMemberStatusChange?: (id: number) => any;
    handleLeaderChange?: (id: any) => any;
}

export default function MemberModule(props: Readonly<IProps>) {

    const addCSSClass = props.additionalClass ? `-${props.additionalClass}` : "";

    function handleMemberStatusChange() {
        if (props.handleMemberStatusChange) {
            props.handleMemberStatusChange(props.memberID);
        }
    }

    function handleLeaderChange() {
        if (props.handleLeaderChange) {
            props.handleLeaderChange(props.memberID)
        }
    }

    return (
        <div
            className={`custom-team-member-container${addCSSClass}`}
        >
            <div
                className="custom-team-member-avatar"
            >
                <div
                    className="custom-team-member-avatar-img"
                    style = {{ backgroundImage: `url(${props.avatar})` }}
                />
            </div>
            <div
                className="custom-team-member-name"
            >
                {props.name}
            </div>
            {
                ((props.isLeader || props.isManager || props.isCreator) && (props.userID !== props.memberID)) &&
                <div
                    className="custom-team-member-buttons"
                >
                    {
                        ((props.isLeader || props.isManager) && props.isManager) &&
                        <button
                            className="custom-button-member"
                            onClick={handleLeaderChange}
                        >
                            Назначить <br />руководителем
                        </button>
                    }
                    {
                        (props.isLeader || props.isCreator) && props.delete &&
                        <div
                            className="custom-icons-cross"
                            onClick={handleMemberStatusChange}
                        />
                    }
                    {
                        (props.isLeader || props.isCreator) && !props.delete &&
                        <div
                            className="custom-icons-plus"
                            onClick={handleMemberStatusChange}
                        />
                    }
                </div>
            }
        </div>
    )
}
