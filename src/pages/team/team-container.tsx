import React, { useEffect, useState } from "react";
import { useHistory, useParams } from "react-router-dom";
import API from "src/api/api";
import ModalWindowModule from "src/common/modules/modal-window/modal-window";
import TeamComponent from "./team-component";

interface IProps {
    user_id: number;
    isManager: boolean;
}

export default function Teams(props: Readonly<IProps>) {

    const { id } = useParams<{ id: string }>();
    const [subdivisionData, setSubdivisionData] = useState<any>();
    // const [subdivisionDashboards, setSubdivisionDashboards] = useState();
    const [isLoaded, setIsLoaded] = useState(false);
    const [members, setMembers] = useState<any>();
    const [membersID, setMembersID] = useState<any>();
    const [leaderID, setLeaderID] = useState(0);
    const [isShowModalAddMember, setShowModalAddMember] = useState(false);
    const [isShowModalCreateDashboard, setIsShowModalCreateDashboard] = useState(false);

    const [teamName, setTeamName] = useState("");

    const history = useHistory();

    async function getSubdivisionInfo(): Promise<void> {
        API.get(
            `subdivision/${id}`
        ).then(
            (response) => {
                const data = response.data.data;
                setMembers(data.members);
                setLeaderID(data.leader.user_id);
                setSubdivisionData(data);
                setTeamName(data.name);

                const membersArrayID = [];
                // tslint:disable-next-line:prefer-for-of
                for (let i = 0; i < data.members.length; i++) {
                    const memberID = data.members[i].user_id;
                    membersArrayID.push(memberID)
                }

                setMembersID(membersArrayID);
                setIsLoaded(true);
            }
        ).catch(
            (error) => {
                console.log(error);
            }
        );

        return
    }

    useEffect(() => {
        getSubdivisionInfo();

    }, []); // eslint-disable-line react-hooks/exhaustive-deps

    function handleOpenModalAddMember() {
        setShowModalAddMember(true);
    }

    function handleCloseModalAddMember() {
        setShowModalAddMember(false);
    }

    function handleOpenModalCreateDashboard() {
        setIsShowModalCreateDashboard(true);
    }

    function handleCloseModalCreateDashboard() {
        setIsShowModalCreateDashboard(false);
    }

    function handleAddMember(value: any) {

        const data = membersID;
        const addID = Number(value);
        data.push(addID);

        const payload = {
            member_id: data
        };

        API.put(
            `subdivision/${id}`,
            payload
        ).then(
            (response) => {
                const membersData = response.data.data.members;
                console.log(response);
                handleCloseModalAddMember();
                setMembers(membersData);
            }
        ).catch(
            (error) => {
                console.log(error);
            }
        )
    }

    function handleDeleteMemberByID(deleteID: number) {

        // tslint:disable-next-line:prefer-const
        let data = membersID;
        const index = data.indexOf(deleteID);

        console.log(deleteID);
        console.log(index);

        if (index !== -1) {
            data.splice(index, 1);
            setMembersID(data);
        }

        const payload = {
            member_id: data
        }

        API.put(
            `subdivision/${id}`,
            payload
        ).then(
            (response) => {
                const membersData = response.data.data.members;
                console.log(response);
                setMembers(membersData);
            }
        ).catch(
            (error) => {
                console.log(error);
            }
        );
    }

    function handleFormTeamNameChange(event: any) {
        const value = event.target.value;
        setTeamName(value);
    }

    function handleFormTeamNameSubmit(event: any) {
        event.preventDefault();

        const payload = {
            name: teamName
        }

        API.put(
            `subdivision/${id}`,
            payload
        ).then(
            (response) => {
                const data = response.data.data;
                setSubdivisionData(data);
            }
        ).catch(
            (error) => {
                console.log(error);
            }
        )
    }

    function handleLeaderChange (value: number) {

        const changeLeader = window.confirm("Вы действительно хотите передать права руководителя?");

        if (changeLeader) {

            const payload = {
                leader_id: value
            }

            API.put(
                `subdivision/${id}`,
                payload
            ).then(
                (response) => {
                    const data = response.data.data;
                    setSubdivisionData(data);
                    alert("Права переданы");
                }
            ).catch(
                (error) => {
                    console.log(error);
                }
            )
        }
    }

    function redirectToDashboard(value: number) {
        history.push(`/user/dashboard/${value}`);
    }

    function handleCreateDashboard(dashboardName: string) {

        const payload = {
            name: dashboardName,
            subdivision_id: subdivisionData.subdivision_id
        }

        API.post(
            "dashboard",
            payload
        ).then(
            async () => {
                await getSubdivisionInfo();
                handleCloseModalCreateDashboard();
            }
        )
    }

    return (
        <>
            {
                isLoaded &&
                <TeamComponent
                    user_id={props.user_id}
                    subdivisionData={subdivisionData}
                    members={members}
                    isLeader={
                        (props.user_id === leaderID)
                    }
                    isManager={props.isManager}
                    isLoaded={isLoaded}
                    formTeamName={teamName}
                    handleOpenModalAddMember={handleOpenModalAddMember}
                    handleOpenModalCreateDashboard={handleOpenModalCreateDashboard}
                    handleDeleteMember={handleDeleteMemberByID}
                    handleFormTeamNameChange={handleFormTeamNameChange}
                    handleFormTeamNameSubmit={handleFormTeamNameSubmit}
                    handleLeaderChange={handleLeaderChange}
                    redirectToDashboard={redirectToDashboard}
                />
            }
            {
                isShowModalAddMember &&
                <ModalWindowModule
                    title="Добавить участника"
                    label="ID пользователя"
                    button="Добавить"
                    handleClose={handleCloseModalAddMember}
                    handleSubmit={handleAddMember}
                />
            }
            {
                isShowModalCreateDashboard &&
                <ModalWindowModule
                    title="Создать доску"
                    label="Название доски"
                    button="Добавить"
                    handleClose={handleCloseModalCreateDashboard}
                    handleSubmit={handleCreateDashboard}
                />
            }
        </>
    );
}
