import React, { useEffect, useState } from "react";
import { useHistory, useParams } from "react-router-dom";
import API from "src/api/api";
import useDebounce from "src/common/scripts/useDebounceHook";
import DashboardTaskViewComponent from "./dashboard-task-component";

interface IProps {
    userData: any;
}

export default function DashboardTaskView(props: Readonly<IProps>) {

    const [task, setTask] = useState<any>({name: ""});
    const [dashboard, setDashboard] = useState<any>({name: "", deadline: 0});
    const [prevTaskName, setPrevTaskName] = useState("");
    const [prevDeadline, setPrevDeadline] = useState(0);
    const [commentText, setCommentText] = useState("");

    const [isLoaded, setIsLoaded] = useState(false);

    const { id, taskID } = useParams<{ id: string, taskID: string }>();
    const history = useHistory();

    const taskName = useDebounce(task.name, 2000);
    const deadline = useDebounce(task.deadline, 10000);

    async function getTaskInfo(): Promise<void> {
        API.get(
            `task/${taskID}`
        ).then(
            (response) => {
                const data = response.data.data;
                setTask(data);
                setPrevTaskName(data.name);
                setPrevDeadline(data.deadline);
                getDashboardInfo();
            }
        ).catch(
            (error) => {
                console.log(error);
            }
        )

        return
    }

    async function getDashboardInfo(): Promise<void> {
        API.get(
            `dashboard/${id}`
        ).then(
            (response) => {
                const data = response.data.data;
                setDashboard(data);
                setIsLoaded(true);
            }
        ).catch(
            (error) => {
                console.log(error);
            }
        )

        return
    }

    useEffect(() => {
        getTaskInfo();
    }, []); // eslint-disable-line react-hooks/exhaustive-deps

    useEffect(() => {
        if (isLoaded && taskName !== prevTaskName && taskName !== "") {
            submitTaskNameChange();
        }

    }, [taskName]); // eslint-disable-line react-hooks/exhaustive-deps

    useEffect(() => {

        if (isLoaded && deadline !== prevDeadline) {
            submitTaskDeadlineChange();
        }

    }, [deadline]); // eslint-disable-line react-hooks/exhaustive-deps

    function submitTaskNameChange() {

        let payloadName = "";
        if (!taskName) {
            payloadName = prevTaskName;
        } else {
            payloadName = taskName;
        }

        const payload = {
            name: payloadName,
            description: task.description,
            deadline: task.deadline,
            column_id: task.column_id,
            executor_id: task.executor_id
        }

        API.put(
            `task/${taskID}`,
            payload
        ).then(
            (response) => {
                const data = response.data.data;
                setTask(data);
                setPrevTaskName(data.name);
            }
        )
    }

    function submitTaskDeadlineChange() {
        const payload = {
            name: task.name,
            description: task.description,
            deadline: deadline,
            column_id: task.column_id,
            executor_id: task.executor.user_id
        }

        API.put(
            `task/${taskID}`,
            payload
        ).then(
            (response) => {
                const data = response.data.data;
                setTask(data);
                setPrevTaskName(data.name);
            }
        )
    }

    function handleCloseTask() {
        history.push(`/user/dashboard/${id}`)
    }

    function handleTaskNameChange(event: React.ChangeEvent<HTMLInputElement>) {
        const value = event.target.value;
        setTask({...task, "name": value})
    }

    function handleTaskDescriptionChange(event: any) {
        const value = event.target.value;
        setTask({...task, "description": value});
    }

    function handleExecutorChange(event: any) {
        const value = event.target.value;

        setTask({...task, executor: {...task.executor, user_id: value}});

        const payload = {
            name: task.name,
            description: task.description,
            deadline: task.deadline,
            column_id: task.column_id,
            executor_id: value
        }

        API.put(
            `task/${taskID}`,
            payload
        ).then(
            (response) => {
                const data = response.data.data;
                setTask(data);
                setPrevTaskName(data.name);
            }
        )
    }

    function handleDeadlineChange(event: any) {
        const value = event.target.value;
        const date = Date.parse(value);

        setTask({...task, "deadline": date});
    }

    function handleChangeCommentText(event: any) {
        const value = event.target.value;
        setCommentText(value);
    }

    function handleCreateComment() {

        const payload = {
            task: task.task_id,
            body: commentText
        }

        API.post(
            `task/${taskID}/comment`,
            payload
        ).then(
            (response) => {
                const data = response.data.data;
                setTask(data);
                setPrevTaskName(data.name);
                setCommentText("");
            }
        )
    }

    if (!isLoaded) {
        return (
            <>
            </>
        )
    } else {
        return (
            <DashboardTaskViewComponent
                task={task}
                members={dashboard.members}
                userData={props.userData}
                createCommentText={commentText}
                handleCloseTask={handleCloseTask}
                handleTaskNameChange={handleTaskNameChange}
                handleTaskDescriptionChange={handleTaskDescriptionChange}
                handleExecutorChange={handleExecutorChange}
                handleDeadlineChange={handleDeadlineChange}
                handleChangeCommentText={handleChangeCommentText}
                handleCreateComment={handleCreateComment}
                isCreator={props.userData.user_id === task.creator.user_id}
                isLeader={props.userData.user_id === task.subdivision.leader_id}
            />
        )
    }
}