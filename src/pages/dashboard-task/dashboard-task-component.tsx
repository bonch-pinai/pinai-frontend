import React, { useEffect, useRef, useState } from "react";
import ModalWindowModule from "src/common/modules/modal-window/modal-window";
import TaskComment from "./modules/comment";
import TextareaAutosize from "react-textarea-autosize";

interface IProps {
    task: any;
    userData: any;
    isCreator: boolean;
    isLeader: boolean;
    members: any;

    createCommentText: string;

    handleCloseTask: () => any;
    handleTaskNameChange: (event: React.ChangeEvent<HTMLInputElement>) => any;
    handleTaskDescriptionChange: (event: any) => any;
    handleExecutorChange: (event: any) => any;
    handleDeadlineChange: (event: any) => any;
    handleChangeCommentText: (event: any) => any;
    handleCreateComment: () => any;
}

export default function DashboardTaskViewComponent(props: Readonly<IProps>) {

    const widthTitleRef = useRef<any>(null);
    const [width, setWidth] = useState(0);

    const style = {
        width: `${width}px`
    }

    const currentDate = new Date();
    const newDate = new Date(currentDate);
    newDate.setHours(currentDate.getHours() + 1);
    const minDeadlineDate = dateToISOLikeButLocal(newDate);

    const date = new Date(props.task.deadline);
    const dateString = date.toLocaleString();
    const dateFormatted = `${dateString.substring(12, 17)} ${dateString.substring(0, 10)}`;

    const inputDate = dateToISOLikeButLocal(date);

    function dateToISOLikeButLocal(d: Date) {
        const offsetMs = d.getTimezoneOffset() * 60 * 1000;
        const msLocal =  d.getTime() - offsetMs;
        const dateLocal = new Date(msLocal);
        const iso = dateLocal.toISOString();
        const isoLocal = iso.slice(0, 16);

        return isoLocal;
    }

    useEffect(() => {

        if (widthTitleRef && widthTitleRef.current) {
            setWidth(widthTitleRef.current.offsetWidth)
        }
    }, [props.task.name]);

    let deadlineElement;
    let descriptionElement;

    if (props.isCreator || props.isLeader) {

        deadlineElement =
            <div className="custom-form-input-wrapper">
                <input
                    type="datetime-local"
                    id="date"
                    onChange={props.handleDeadlineChange}
                    value={inputDate}
                    min={minDeadlineDate}
                    required
                />
            </div>;

        descriptionElement =
            <TextareaAutosize
                id="description"
                value={props.task.description}
                onChange={props.handleTaskDescriptionChange}
            />
    } else {

        deadlineElement =
            <div
                className="custom-dashboard-task-body-col-1-top-deadline-content-time"
            >
                {dateFormatted}
            </div>;

        descriptionElement =
            <div
                className="custom-dashboard-task-body-description"
            >
                {props.task.description}
            </div>
    }

    return (
        <div
            className="custom-dashboard-task"
        >
            <ModalWindowModule
                handleClose={props.handleCloseTask}
            >
                <div
                    className="custom-dashboard-task-container"
                >
                    <div
                        className="custom-dashboard-task-title"
                    >
                        {
                            (props.isCreator || props.isLeader) &&
                            <input
                                className="custom-input-invisible"
                                value={props.task.name}
                                style={style}
                                onChange={props.handleTaskNameChange}
                            />
                        }
                        {
                            (props.isCreator || props.isLeader) &&
                            <div
                                className="custom-input-invisible-ctrl"
                                ref={widthTitleRef}
                            >
                                {props.task.name}
                            </div>
                        }
                        {
                            !(props.isCreator || props.isLeader) &&
                            <div
                                className="custom-dashboard-topbar-title-name"
                            >
                                {props.task.name}
                            </div>
                        }
                    </div>
                    <div
                        className="custom-dashboard-task-body"
                    >
                        <div
                            className="custom-dashboard-task-body-col-1"
                        >
                            <div
                                className="custom-dashboard-task-body-col-1-top"
                            >
                                {
                                    (props.task.executor || (props.isCreator || props.isLeader)) &&
                                    <div
                                        className="custom-dashboard-task-body-col-1-top-executor"
                                    >
                                        <div
                                            className="custom-dashboard-task-section-title"
                                        >
                                            Исполнитель
                                        </div>
                                        <div
                                            className="custom-dashboard-task-body-col-1-top-executor-content"
                                        >
                                            {
                                                !(props.isCreator || props.isLeader) &&
                                                <>
                                                    <div
                                                        className="custom-sidebar-user-avatar"
                                                        style =
                                                            {{ backgroundImage: `url(${props.task.executor.avatar})` }}
                                                    />
                                                    <span>
                                                        {props.task.executor.full_name}
                                                    </span>
                                                </>
                                            }
                                            {
                                                (props.isCreator || props.isLeader) &&
                                                <div
                                                    className="custom-select-wrapper"
                                                >
                                                    <select
                                                        id="executorID"
                                                        onChange={props.handleExecutorChange}
                                                        value={
                                                            props.task.executor ?
                                                            props.task.executor.user_id :
                                                            0
                                                        }
                                                        required
                                                    >
                                                        <option
                                                            value={0}
                                                        />
                                                        {
                                                            props.members.map((
                                                                member: {
                                                                    avatar: string;
                                                                    full_name: string;
                                                                    user_id: number
                                                                },
                                                                index: number) => {
                                                                    return (
                                                                        <option key={index} value={member.user_id}>
                                                                            {member.full_name}
                                                                        </option>
                                                                    )
                                                                }
                                                            )
                                                        }
                                                    </select>
                                                </div>
                                            }
                                        </div>
                                    </div>
                                }
                                <div
                                    className="custom-dashboard-task-body-col-1-top-deadline"
                                >
                                    <div
                                        className="custom-dashboard-task-section-title"
                                    >
                                        Срок
                                    </div>
                                    <div
                                        className="custom-dashboard-task-body-col-1-top-deadline-content"
                                    >
                                        {deadlineElement}
                                    </div>
                                </div>
                            </div>
                            <span className="custom-dashboard-task-section-title">
                                Описание
                            </span>
                            <div
                                className="custom-textarea-wrapper"
                            >
                                {descriptionElement}
                            </div>
                            <span className="custom-dashboard-task-section-title">
                                Комментарии
                            </span>
                            <div
                                className="custom-dashboard-task-comment-create-wrapper"
                            >
                                <div
                                    className="custom-dashboard-task-comment-create"
                                >
                                    <div
                                        className="custom-dashboard-task-comment-user"
                                    >
                                        <div
                                            className="custom-sidebar-user-avatar"
                                            style = {{ backgroundImage: `url(${props.userData.avatar})` }}
                                        />
                                    </div>
                                    <div
                                        className="custom-textarea-wrapper"
                                    >
                                        <TextareaAutosize
                                            id="description"
                                            value={props.createCommentText}
                                            placeholder={"Напишите комментарий..."}
                                            onChange={props.handleChangeCommentText}
                                        />
                                    </div>
                                </div>
                                {
                                    props.createCommentText &&
                                    <button
                                        className="custom-button-8"
                                        onClick={props.handleCreateComment}
                                    >
                                        Отправить
                                    </button>
                                }
                            </div>
                            {
                                props.task.comments
                                .sort((a: any, b: any) => b.updated_at - a.updated_at)
                                .map((
                                comment: {
                                    comment_id: number;
                                    body: string;
                                    creator: {
                                        avatar: string;
                                        full_name: string;
                                        user_id: number;
                                    };
                                    updated_at: number;

                                },
                                index: number) => {

                                return (
                                    <div
                                        key={index}
                                        className="custom-dashboard-task-comment"
                                    >
                                        <TaskComment
                                            comment_id={comment.comment_id}
                                            body={comment.body}
                                            creator={comment.creator}
                                            updated_at={comment.updated_at}
                                        />
                                    </div>
                                );
                            })
                        }
                        </div>
                        {/* {
                            (props.isCreator || props.isLeader) &&
                            <div
                                className="custom-dashboard-task-body-col-2"
                            >
                                a
                            </div>
                        } */}
                    </div>

                </div>
            </ModalWindowModule>
        </div>
    )
}