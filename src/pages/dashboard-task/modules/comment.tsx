import React from "react";

interface IProps {
    comment_id: number;
    body: string;
    creator: {
        avatar: string;
        full_name: string;
        user_id: number;
    };
    updated_at: number;
}

export default function TaskComment(props: Readonly<IProps>) {

    const date = new Date(props.updated_at);
    const dateString = date.toLocaleString();
    const dateFormatted = `${dateString.substring(12, 17)} ${dateString.substring(0, 10)}`;

    return (
        <div
            className="custom-dashboard-task-comment-container"
        >
            <div
                className="custom-dashboard-task-comment-user"
            >
                <div
                    className="custom-sidebar-user-avatar"
                    style = {{ backgroundImage: `url(${props.creator.avatar})` }}
                />
                <span className="custom-dashboard-task-comment-user-name">
                    {props.creator.full_name}
                </span>
                <span className="custom-dashboard-task-comment-date">
                    {dateFormatted}
                </span>
            </div>
            <div
                className="custom-dashboard-task-comment-body"
            >
                {props.body}
            </div>
        </div>
    )
}