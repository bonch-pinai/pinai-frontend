import React, { useState, useEffect } from "react";
import ForgotComponent from "./forgot-component";
import API from "../../api/api";
import validation from "../../common/scripts/validation";

interface IForgotFormData {
    email: string;
}

export default function Forgot() {

    const initData = {
        email: ""
    }

    const [formData, setFormData] = useState<IForgotFormData>(initData);
    const [formErrorData, setFormErrorData] = useState<IForgotFormData>(initData);
    const [showError, setShowError] = useState(false);
    const [isMessageSent, setIsMessageSent] = useState(false);

    useEffect(() => {
        validation(formData, formErrorData, "email", formData.email, setFormErrorData);
        setShowError(false);
    },
        // eslint-disable-next-line
        [formData]
    );

    function handleChange(event: React.ChangeEvent<HTMLInputElement>): void {
        const target = event.target;
        const value = target.value;
        setFormData({...formData, [target.id]: value});
    }

    function handleSubmit(event: React.FormEvent<HTMLFormElement>): void {
        event.preventDefault();

        const checkForm = Object.values(formErrorData).every(value => value === "");
        if (checkForm) {
            API.post(
                "password/forgot",
                formData
            ).then(
                (response) => {
                    console.log(response);
                    setIsMessageSent(true);
                }
            ).catch(
                (error) => {
                    console.log(error);
                    setFormErrorData({"email": "Аккаунта с данной почтой не существует"});
                    setShowError(true);
                }
            )
        } else {
            setShowError(true);
        }
    }

    return (
        <ForgotComponent
            isMessageSent={isMessageSent}
            formData={formData.email}
            formErrorData={formErrorData.email}
            showError={showError}
            handleSubmit={handleSubmit}
            handleChange={handleChange}
        />
    );
}