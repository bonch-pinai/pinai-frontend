import React from "react";
import { Link } from "react-router-dom";

interface Props {
    isMessageSent: boolean;
    formErrorData: string;
    formData: string;
    showError: boolean;
    handleSubmit: (event: any) => any;
    handleChange: (event: React.ChangeEvent<HTMLInputElement>) => any;
}

export default function ForgotComponent (props: Readonly<Props>) {

    if (!props.isMessageSent) {

        return (

            <div
                className="custom-recover"
            >
                <form
                    className="custom-form"
                    onSubmit={props.handleSubmit}
                >
                    <span className="custom-form-title">
                        Восстановление пароля
                    </span>
                    <div
                        className="custom-form-input-wrapper"
                    >
                        <input
                            type="text"
                            id="email"
                            onChange={props.handleChange}
                            required
                            autoFocus
                        />
                        <label
                            htmlFor="email"
                        >
                            Укажите адрес электронной почты
                        </label>
                        {
                            (props.formErrorData.length > 0 && props.showError && props.formData) &&
                            <span className="custom-form-error">
                                {props.formErrorData}
                            </span>
                        }
                    </div>
                    <button
                        className="custom-button"
                        type="submit"
                    >
                        Далее
                    </button>
                    <hr />
                    <Link
                        to=""
                        className="custom-link"
                    >
                        Назад
                    </Link>
                </form>
            </div>
        )
    } else {

        return (
            <div
                className="custom-recover"
            >
                <form
                    className="custom-form"
                >
                    <span className="custom-form-title">
                        Восстановление пароля
                    </span>
                    <div className="custom-form-info-done">
                        На почту <span>{props.formData}</span> было отправлено письмо для сброса пароля
                    </div>
                    <hr />
                    <Link
                        to=""
                        className="custom-link"
                    >
                        Вернуться ко входу
                    </Link>
                </form>
            </div>
        )
    }
}
