import React, { useEffect } from "react";
import EmailConfirmedComponent from "./email-confirmed-component";
import { useHistory } from "react-router-dom";
import API from "src/api/api";

export default function EmailConfirmed() {

    const history = useHistory();

    useEffect(() => {

        API.get(
            "check"
        ).then(
            (result: any) => {
                const response = JSON.parse(result);
                if (response.alert && response.email) {
                    const email = response.email;
                    history.push({
                        pathname: "/email-resend-confirm",
                        state: {email}
                    });
                }
            }
        ).catch((error) => {
            console.log(error)
        });
    });

    return (
        <EmailConfirmedComponent />
    );
}
