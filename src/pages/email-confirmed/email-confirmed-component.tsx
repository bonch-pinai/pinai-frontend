import React from "react";
import { Link } from "react-router-dom";

export default function EmailConfirmedComponent() {

    return (
        <div
            className="custom-recover"
        >
            <form
                className="custom-form"
            >
                <span className="custom-form-title">
                    Подтверждение почты
                </span>
                <div className="custom-form-info">
                    Ваш почтовый адрес был успешно подтверждён!
                </div>
                <hr />
                <Link
                    to=""
                    className="custom-link"
                >
                    Вернуться ко входу
                </Link>
            </form>
        </div>
    );
}
