import React, { useEffect } from "react";
import API from "./api/api";
import AppComponent from "./App-component";

function App() {

    useEffect(() => {
        const isCookieCSRF = checkCookieCSRF();

        if (!isCookieCSRF) {
            getCookieCSRF();
        }

    }, []);

    function checkCookieCSRF(): boolean {
        if (document.cookie.split(";").filter((item) => item.trim().startsWith("XSRF-TOKEN=")).length) {
            return true
        } else {
            return false
        }

    }

    function getCookieCSRF(): void {

        API.get(
            "sanctum/csrf-cookie"
        ).catch((error) => {
            console.log(error)
        });
    }

  return (
    <AppComponent
    />
  );
}

export default App;
