import React from "react";
import {
  BrowserRouter as Router,
  Switch,
  Route
} from "react-router-dom";
import Registration from "./pages/registration-form/registration-container";
import Login from "./pages/login-form/login-container";
import Forgot from "./pages/forgot-password/forgot-container";
import Reset from "./pages/reset-password/reset-container";
import EmailResend from "./pages/email-resend/email-resend-container";
import UserHome from "./pages/user/user-home-container";
import EmailConfirmed from "./pages/email-confirmed/email-confirmed-container";

function AppComponent() {

  return (
    <div className="App">
      <Router>
        <Switch>
            <Route exact path = "/" component={Login} />
            <Route exact path = "/signup" component={Registration} />
            <Route exact path = "/forgot" component={Forgot} />
            <Route exact path = "/email-resend-confirm" component={EmailResend} />
            <Route path = "/auth/reset-password" component={Reset} />
            <Route path = "/user" component={UserHome} />
            <Route path = "/email-confirmed" component={EmailConfirmed} />
        </Switch>
      </Router>
    </div>
  );
}

export default AppComponent;
